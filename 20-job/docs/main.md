




# JOB: Looking for appropriate parameter

## Total charge on each rod

### Variable

Number of unit charges on each rod is characterized by variale
`$num_rand_shell`.


### Trial 1

#### Parameters

* charge: `$num_rand_shell` $= 5\times10^6$
* Hamaker: `$hmk` $= 4.23\times10^{-19}$

#### Results

* Electrostatic

\begin{figure}[h!] 
\centering 
\begin{multicols}{3}
    \includegraphics[scale=0.25]{../../22-results/22-electrostatic.png} \par 
    \includegraphics[scale=0.25]{../../22-results/22-vdw.png} \par 
    \includegraphics[scale=0.25]{../../22-results/22-total.png} \par 
\end{multicols}
\end{figure}



