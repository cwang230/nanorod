#!/usr/bin/env perl 

use strict;
use warnings;
use feature qw(say);
use POSIX;                      # floor function...
use List::MoreUtils 'pairwise'; # pairwise addition...
use Data::Dumper;               # show data hierarchy...

#=========================================
# (:-> 
#  d:distance between two grid points,
#  R:radius of the circle,
#  C:center of the circle,
#  H:heght of the circle,
# )^<setup>
# -->
# n:number of intervals between two points horizontally
# = (R/d)^<floor>
# m:number of intervals between two points vertically
# = (H/d)^<floor>
#=========================================

# parameter setup...
my $d = 1.4;       # the distance between points...
my $R = 17.0/2.0;
my $H = 22.0;
my $s = 8.8;       # separation between two rods...
# my $s = 5.4;       # separation between two rods...
# my @C = (0.0, $s/2.0+$R ,0.0);
my @C = (0.0, -1*$s/2.0-$R ,0.0);
my $n = floor(2*$R/$d); # max number of points along radius... 
my $m = floor($H/$d); # number of layers... 

# Create an array for rectangle...
my @rectangle = ();  # coordinates of rectangle grid...

# need an array to store coordinates for all 
# candidate coordinates horizontally...
my @coordinates = ();

# at least this many number of points are needed...
foreach my $index (0..$n) {  
	push @coordinates, (-$n/2+$index)*$d;
}
#^t: print Dumper(\@coordinates);

# coordinates vertically...
my @heights = ();
foreach my $index (0..$m) {
	push @heights, (-$m/2+$index)*$d;
}
#^t: print Dumper(\@heights);

# pick up coordinates from @coordinates 
# and form array rectangle...
foreach my $height (@heights) {
	foreach my $row (@coordinates) {
		push @rectangle, [           # each array reference here represents coordinate of a point...
			map {
				$_**2 + $row**2 < $R**2 ?   # filter those which are not in the circle...

				[
					pairwise {$a+$b}       # pairwise addition from two arrays below...
					@{[$_,$row,$height]},  # dereference version:sandwich version of array...
					@C
				]        :

				[ ]                      # otherwise, return an empty array reference...
			} 
			@coordinates

		];
	}
}
# print Dumper([@rectangle[0..10]]);

# count number of points in one layer...
# my $nlayer = 0;
# my $layer_id = 0;
# while ($nlayer == 0) {
# 	$nlayer = scalar 
# 						grep {@{$_} > 0} @{$rectangle[$layer_id]};
# 	$layer_id += 1 if $layer_id < length @rectangle;
# }

# print information about system for analysis...
# say "Number of points in one layer => ", $nlayer;
say "Number of layers => ", $m+1;

# That's it...
# output it...

open(my $fh, ">coords_right.txt");
# open(my $fh, ">coords_left.txt");
foreach my $L1 (@rectangle) {
	foreach my $L2 (@$L1) {
			#say ($L2,$L1);
			(say $fh join ' ', @$L2) if @$L2 != 0;
	}
}
close($fh);
