#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                      # floor function...
use List::MoreUtils 'pairwise'; # pairwise addition...
use Data::Dumper;               # show data hierarchy...
use List::Util qw(sum reduce);
use List::Flatten;
use File::Slurp;                # enable file slurp during input...
use Math::MatrixReal;
use Math::Trig;
use Ref::Util qw(is_arrayref);                  # test if object is an array reference...
use Nanorod;

BEGIN {$| = 1}

my $experiment = "out";

# set up the initial distance for initial coordinates of cylinders and shells...

my $init_distance = 3.0;
my $final_distance = 6.0;

#-----------------------------------
# set up cylidner, shell coordinates
#-----------------------------------
                                         
# parameters...

my $ligand_size = 0.00;  # nm
my $separation_shell = $init_distance - 2*$ligand_size;
my $separation = $init_distance;
my $radius = 0.1;  # nm
my $height = 1.2;      # nm
my $grid_size = 0.1;    # nm
my %center = (
				left  => [0.0, -1*$separation/2.0-$radius, 0.0],
				right => [0.0,  1*$separation/2.0+$radius, 0.0],
);
my $num_rand_shell = 0;    # total number of random points...           

# generate <shell> and <cylinder>...

my $cylinder_left = [];
my $cylinder_right = [];

cylinder_coordinates_gen({
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{left},
				cylinder => $cylinder_left,
});

cylinder_coordinates_gen({
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{right},
				cylinder => $cylinder_right,
});

my $dir = "../results-out-try/";
write_to_file({array=>$cylinder_right,file=>$dir."cylinder_right.txt"});
write_to_file({array=>$cylinder_left,file=>$dir."cylinder_left.txt"});

#--------------------------
# set up specs for solution
#--------------------------
                                              
my %idls = (         # inverse Debye length...
				in => 1.38042,   # nm^-1             
				out => 0.52464,  # nm^-1             
);                                       
                                         
my $idl = $idls{$experiment};
my $hmk = 3e-19; # Hamaker constant => J
                                         
# electric constants...
my $epsilon0 = 8.85e-21;   # F nm^-1...
my %epsilon = (            # dielectric constant...
				in  => 80,               # unit.? 
				out => 80,
);
my $pts_per_cylinder = scalar @{ $cylinder_left };
my %sigmas = (                       # charges for each point...
				in  => 1*1.6e-19, 
				out => 1*1.6e-19,
);
my $sigma = $sigmas{$experiment};    
my $joule2eV = 6.242e18;   # conversion between joule to eV...
my %epss = (               # the constant part of electrostatic...
				in  => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{in})**(-1),
				out => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{out})**(-1),
);
my $eps = $epss{$experiment};
say "eps --> ".$eps;


# compute the interaction_dd...
my $interaction_dd = 0;
foreach my $p1 (@{$cylinder_left}) {
				foreach my $p2 (@{$cylinder_right}) {

								# interaction_dd calculated...
								$interaction_dd += 
									$joule2eV * (
										$eps*dd($p1,$p2,$idl)                   # dd interaction_vdw...    
									);

				}
}

say "Electrostatic --> ".$interaction_dd;

END {$| = 0}

#===========
# subroutine
#===========

sub rotate_y {
	
	my ($input) = @_;

	my $angle = $input->{angle} // 0.0;
	my $vector = [$input->{vector}] // [[0.0, 0.0, 0.0]];

	my $vector_mat = Math::MatrixReal->new_from_cols($vector);

	my $rot_mat = Math::MatrixReal->new_from_rows(
		[
			[cos($angle), 0.0, sin($angle)],
			[0.0,         1.0, 0.0        ],
			[-sin($angle),0.0, cos($angle)],
		]
	);

	my $new_vector_mat = $rot_mat->multiply($vector_mat);
	return (~$new_vector_mat)->[0]->[0];
}

sub dd {
	my ($p1, $p2, $idl) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];
	$idl //= 0.0; # inverse debye length...

	return exp(-1*$idl*dist($p1,$p2)) 
				 * 1.0 
				 / dist($p1,$p2);
}

# vdw interaction...
sub vdw {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return dist($p1,$p2)**(-6);
}

sub dist {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return 
		sqrt 
		sum
		pairwise { ($a-$b)**2 } @{$p1}, @{$p2};
}

sub cylinder_coordinates_gen {

	# set up the input...
	my ($input) = @_;     # input with hash...

	# parameter setup...
	my $d = $input->{grid_size}   // 1.4;       # the grid_size between points...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;       # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my @rectangle = ();  # coordinates of rectangle grid...

	# figure out properties of cylinders...
	my $n = floor(2*$R/$d); # max number of points along radius... 
	my $m = floor($H/$d); # number of layers... 

	# need an array to store coordinates for all 
	# candidate coordinates horizontally...
	my @coordinates = ();

	# at least this many number of points are needed...
	foreach my $index (0..$n) {  
		push @coordinates, (-$n/2+$index)*$d;
	}

	# coordinates vertically...
	my @heights = ();
	foreach my $index (0..$m) {
		push @heights, (-$m/2+$index)*$d;
	}

	# say Dumper $C;

	# pick up coordinates from @coordinates 
	# and form array rectangle...
	foreach my $height (@heights) {
		foreach my $row (@coordinates) {
			push @rectangle, [           # each array reference here represents
			                             # coordinate of a point...
				map {
					$_**2 + $row**2 < $R**2 ?   # filter those which are not in the circle...

					[
						pairwise {$a+$b}       # pairwise addition from two arrays below...
						@{[$_,$row,$height]},  # dereference version:sandwich version of array...
						@{$C}
					]        :

					[ ]                      # otherwise, return an empty array reference...
				} 
				@coordinates

			];
		}
	}

	# print information about system for analysis...
#	say "Number of layers => ", $m+1;

	push @{ $input->{cylinder} }, 
			 grep { scalar @{ $_ } > 0 } # remove empty list...
       flat @rectangle;

	return 0;
}

sub shell_coordinates_gen {

	# set up input...
	my ($input) = @_;

	# parameter setup...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;                        # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my $N = $input->{points}     // 100;
	my @rectangle = ();  # coordinates of rectangle grid...

	# need an array to store coordinates for all 
	my @coordinates = ();
	my $angle = 0;

	foreach my $id (0..$N-1) {
		$angle = rand(1)*2*pi;
		push @coordinates, [
			pairwise {$a+$b} 
				@{[
					$R * cos($angle),
					$R * sin($angle),
					$H*rand(1) - $H/2, 
				]},
				@{$C}
		];
	}

	# output...
	push @{ $input->{shell} }, @coordinates;

	return 0;
}

sub coordinates_move {
				
				my ($input) = @_;
				
				my $current_coordinates = $input->{coordinates} // [];    # 
				my $path_vector = $input->{path_vector} // [0,0,0];

				# move by vector...
				foreach my $point (@{ $current_coordinates }) {
								
								$point = [
												pairwise {$a+$b} 
																@{ $point }, @{ $path_vector }
								];

				}

				return 0;
}

sub write_to_file {
	
		my ($input) = @_;

		my $array_rf   = $input->{array};         # input array...
		my $write_file = $input->{file};          # file to write... 

		open (my $write_fh, ">".$write_file);

		my $current_data = undef;
		foreach my $current_item (@{$array_rf}) {
				
				$current_data = is_arrayref($current_item)?
												join " ", @{$current_item} :
												$current_item;

				# if it is a reference...
				print $write_fh $current_data."\n";

		}

		close($write_fh);

		return 0;
}
