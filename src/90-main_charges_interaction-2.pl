#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use Math::Trig;
use Nanorod;

BEGIN {$| = 1}

my $experiment = "out";

# set up the initial distance for initial coordinates of cylinders and shells...

my $init_distance = 3.0;
my $final_distance = 6.0;

#-----------------------------------
# set up cylidner, shell coordinates
#-----------------------------------
                                         
# parameters...

my $ligand_size = 0.00;  # nm
my $separation_shell = $init_distance - 2*$ligand_size;
my $separation = $init_distance;
my $radius = 0.1;  # nm
my $height = 0.1;      # nm
my $grid_size = 0.1;    # nm
my %center = (
				left  => [0.0, -1*$separation/2.0-$radius, 0.0],
				right => [0.0,  1*$separation/2.0+$radius, 0.0],
);
my $num_rand_shell = 0;    # total number of random points...           

# generate <shell> and <cylinder>...

my $cylinder_left = [];
my $cylinder_right = [];

cylinder_coordinates_gen({
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{left},
				cylinder => $cylinder_left,
});

cylinder_coordinates_gen({
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{right},
				cylinder => $cylinder_right,
});

my $dir = "../results-out-try/";
write_to_file({array=>$cylinder_right,file=>$dir."cylinder_right.txt"});
write_to_file({array=>$cylinder_left,file=>$dir."cylinder_left.txt"});

#--------------------------
# set up specs for solution
#--------------------------
                                              
my %idls = (         # inverse Debye length...
				in => 1.38042,   # nm^-1             
				out => 0.52464,  # nm^-1             
);                                       
                                         
my $idl = $idls{$experiment};
my $hmk = 3e-19; # Hamaker constant => J
                                         
# electric constants...
my $epsilon0 = 8.85e-21;   # F nm^-1...
my %epsilon = (            # dielectric constant...
				in  => 80,               # unit.? 
				out => 80,
);
my $pts_per_cylinder = scalar @{ $cylinder_left };
my %sigmas = (                       # charges for each point...
				in  => 12*1.6e-19, 
				out => 12*1.6e-19,
);
my $sigma = $sigmas{$experiment};    
my $joule2eV = 6.242e18;   # conversion between joule to eV...
my %epss = (               # the constant part of electrostatic...
				in  => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{in})**(-1),
				out => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{out})**(-1),
);
my $eps = $epss{$experiment};
say "eps --> ".$eps;


# compute the interaction_dd...
my $interaction_dd = 0;
foreach my $p1 (@{$cylinder_left}) {
				foreach my $p2 (@{$cylinder_right}) {

								# interaction_dd calculated...
								$interaction_dd += 
									$joule2eV * (
										$eps*dd($p1,$p2,$idl)                   # dd interaction_vdw...    
									);

				}
}

say "Electrostatic --> ".$interaction_dd;

END {$| = 0}

