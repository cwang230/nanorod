#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                           # floor function...                                 
use List::MoreUtils 'pairwise';      # pairwise addition...                              
use Data::Dumper;                    # show data hierarchy...                            
use List::Util qw(sum reduce);                                                          
use List::Flatten;                                                                      
use File::Slurp;                     # enable file slurp during input...                 
use Math::MatrixReal;                                                                   
use Math::Trig;                                                                         
use Ref::Util qw(is_arrayref);       # test if object is an array reference...           
use Nanorod;

my $vector = [-1,0,0];
rotate_axis({
				angle => -pi/4.0,
				vector=> \$vector,
				axis  => "y",
});

say Dumper $vector;



say eta_charge(10,22.0);
