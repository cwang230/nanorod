!=======================================================================
      module procedures
      implicit none
      integer, parameter :: dp = selected_real_kind(14)      

      !----------------------------------------------------------
      ! Atomistic model
      !  core side length = 137.85 A
      !  ligand thickness = 13.5 A (max 20 A)
      ! Experiment
      !  avg core side length = 133.7 A
      !  avg NP-NP dist = 29.9 A
      !----------------------------------------------------------
      ! NP core side length, NP core face-to-face distance
      real(dp),parameter :: a=133.7_dp, rm=29.9_dp ! for exp vdW
      ! Number of atoms, beads, output pt, surface pt, NP
      integer,parameter  :: NA=24000
      integer,parameter  :: NB=10,NO=10,NS=386,NV=27,NP=NA/NB
      !----------------------------------------------------------
      ! Universal constants
      real(dp),parameter :: PI=3.141592653589793_dp
      real(dp),parameter :: Temp=48._dp ! Temperature
      real(dp),parameter :: kT=0.0019872041_dp*Temp ! kcal/mol
      !----------------------------------------------------------
      ! External magnetic field
      ! real(dp),parameter :: Hs(3)=[ 1200._dp, 0._dp, 0._dp ] !unit G
      real(dp) :: Hs(3)=[ 1200._dp, 0._dp, 0._dp ] !unit G
      !----------------------------------------------------------
      ! Energies, unit: kcal/mol
      real(dp),parameter :: KvdW=1.E12_dp*0.2_dp*0.25_dp
      real(dp),parameter :: Ka=-3.784_dp*1._dp
      real(dp),parameter :: Kdd=18935560.34_dp
      real(dp),parameter :: Kz=-0.207_dp/4._dp/PI
      !----------------------------------------------------------
      ! Periodic System, Output
      ! if it is a continued job, set startfrom to be non-zero
      integer,parameter  :: startfrom=0, run=4800 ! number of mc cycle
      ! integer,parameter  :: startfrom=0, run=4800 ! number of mc cycle
      integer,parameter  :: burnin=2000, burnfreq=10
      !----------------------------------------------------------
      ! Variable step size
      real(dp),parameter :: target_rate=0.5_dp
      ! Variable step size (NP trans and rot) n=min, M=Max, f=freq
      real(dp)           :: ds = 6.65_dp
      real(dp)           :: da  = 0.082_dp
      real(dp),parameter :: dsn =1.0_dp   , dan =0.017_dp
      real(dp),parameter :: dsM =100._dp  , daM =0.087_dp
      integer,parameter  :: dsf=1000      , daf=1000      
      !----------------------------------------------------------
      ! > maxD2 = no overlap, < minD2 = overlap for sure
      real(dp),parameter :: minD2 = a**2._dp
      real(dp),parameter :: maxD = 0.72112479_dp*a*2._dp+rm
      real(dp),parameter :: maxD2 = maxD**2._dp
      real(dp),parameter :: neighD2 = (6._dp*(a+rm)+1._dp)**2._dp !(maxD+411._dp)**2._dp
!      real(dp),parameter :: eps=60000._dp , AHam=2.08_dp*50._dp
!      real(dp),parameter :: a6 = a**6._dp, PI2 = PI*PI, NV2 = NV*NV
!      real(dp),parameter :: Kattr = ( a6*0.81_dp*Aham/(NV2*PI2) )
!      real(dp),parameter :: Krepul = eps*(13.6_dp**12._dp)
!      real(dp),parameter :: Kfar = -Aham*(0.81_dp*a6)/PI2
      !----------------------------------------------------------
      ! Variables for routines (read file, loop, etc...)
      character*30 junk,outfile,infile
      integer  :: i,j,k,iNP,imc,io
      real(dp) :: I4by4(4,4),I3by3(3,3)
      ! Import 
      character*3 :: Oname(10) =                                        &
     &  ['FC ','FX1','FX2','FY1','FY2','FZ1','FZ2','FV1','FV2','FD ']
      integer  :: NPs(NP) = [ (i,i=1,NP) ] ! list of NPs
      real(dp) :: NPxyz(4,NB,NP)
      real(dp) :: Sxyz(4,NS),Sarea(NS),Oxyz(4,NO),Vxyz(4,NV)
      ! Structures
      real(dp) :: T0np(4,4,NP),T1np(4,4,NP),T2np(4,4,NP)
      real(dp) :: rd4(4)
      real(dp) :: Ms0(3,NP),Ms1(3,NP),Ms2(3,NP)
      ! Energies
      real(dp) :: Eall(4,2),Esys(4)
      ! For testing purposes
      integer :: clock_rate,clock_start,clock_stop
      integer :: propose(2),accept(2)
      real(dp) :: rate(2)
      logical :: updated
      real(dp) :: accrate
      logical :: NotNeigh1(NP,NP)=.False.,NotNeigh2(NP,NP)=.False.
      logical :: NotNeigh0(NP,NP)=.False.
      real(dp) :: surf(NS)

      real(dp) :: pt(4)
      contains
!=======================================================================
      function norm(v1)
      real(dp),intent(in) :: v1(:)
      real(dp) :: norm(size(v1))
      ! Return normalized v1
      norm = v1/dsqrt(sum(v1*v1))
      return
      end function norm
!=======================================================================
      function cross(v1,v2)
      real(dp) :: cross(3)
      real(dp),intent(in) :: v1(:),v2(:)
      ! cross product of v1 and v2 (v1 x v2)
      cross(1) = v1(2)*v2(3)-v1(3)*v2(2)
      cross(2) = v1(3)*v2(1)-v1(1)*v2(3)
      cross(3) = v1(1)*v2(2)-v1(2)*v2(1)
      return
      end function cross
!=======================================================================
      function qconj(q1)
      real(dp) :: qconj(4)
      real(dp),intent(in) :: q1(:)
      ! Return conjugate of quaternion
      qconj = [ q1(1), -q1(2:4) ]
      return
      end function qconj
!=======================================================================
      function qqmul(q1,q2)
      real(dp) :: qqmul(4)
      real(dp),intent(in) :: q1(:),q2(:)
      ! quaternion = [ w, v_ijk ]
      ! quaternion product = 
      !       w1*w2 - dot(v1,v2), w1*v2 + w2*v1 + cross(v1,v2)
      qqmul(1) = q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3) - q1(4)*q2(4)
      qqmul(2) = q1(1)*q2(2) + q1(2)*q2(1) + q1(3)*q2(4) - q1(4)*q2(3)
      qqmul(3) = q1(1)*q2(3) - q1(2)*q2(4) + q1(3)*q2(1) + q1(4)*q2(2)
      qqmul(4) = q1(1)*q2(4) + q1(2)*q2(3) - q1(3)*q2(2) + q1(4)*q2(1)
      return
      end function qqmul
!=======================================================================
      function qmat(q1)
      real(dp) :: qmat(3,3)
      real(dp),intent(in) :: q1(:)
      real(dp) :: w,X,Y,Z,Nq,s,wX,wY,wZ,xX,xY,xZ,yY,yZ,zZ
      ! convert quaternion q1 to rotational matrix
      ! http://en.wikipedia.org/wiki/Rotation_matrix#Quaternion
      Nq = sum(q1*q1) ; s = 0._dp
      if ( Nq > 0._dp ) s = 2/Nq
       X = q1(2)*s;  Y = q1(3)*s;  Z = q1(4)*s
      wX = q1(1)*X; wY = q1(1)*Y; wZ = q1(1)*Z
      xX = q1(2)*X; xY = q1(2)*Y; xZ = q1(2)*Z
      yY = q1(3)*Y; yZ = q1(3)*Z; zZ = q1(4)*Z
      qmat(1,1)=1.0-(yY+zZ);qmat(1,2)=xY-wZ      ;qmat(1,3)=xZ+wY
      qmat(2,1)=xY+wZ      ;qmat(2,2)=1.0-(xX+zZ);qmat(2,3)=yZ-wX
      qmat(3,1)=xZ-wY      ;qmat(3,2)=yZ+wX      ;qmat(3,3)=1.0-(xX+yY)
      return
      end function qmat
!=======================================================================
      function qalign(vi,vf)
      real(dp) :: qalign(3,3)
      real(dp),intent(in) :: vi(3),vf(3)
      real(dp) :: ang
      ! return rotation matrix that will align vi with vf
      ang = dacos(max(-1.0_dp,min(1.0_dp,sum(norm(vi)*norm(vf)))))
      qalign = qRot(norm(cross(vi,vf)),ang)
      return
      end function qalign
!=======================================================================
      function qRot(vax,ang)
      real(dp) :: qRot(3,3)
      real(dp),intent(in) :: vax(:),ang
      real(dp) :: halfang
      ! return rotation matrix that cc-rotate ang along axis vax
      halfang = ang/2._dp
      qRot = qmat( [ dcos(halfang), dsin(halfang)*vax ] )
      return
      end function qRot
!=======================================================================
      function RdC()
      real(dp) :: RdC(3)
      real(dp) :: Rd(2),s
      ! random point in 2D circle
      s=9._dp ! reset
      do while (s >= 1._dp) ! exit when x^2+y^2 = s < 1
       call random_number(Rd)
       Rd = 2._dp*Rd-1._dp
       s = sum(Rd*Rd)
      enddo
      RdC = [ Rd(1), Rd(2), s ] ! x, y, x^2+y^2
      return
      end function
!-----------------------------------------------------------------------
      function RdV()
      real(dp) :: RdV(3)
      real(dp) :: Rd(3),s
      ! uniformly pick a random point on 3D unit sphere
      ! see paper
      ! "Choosing a point from the surface of a sphere"
      ! by George Marsaglia
      Rd = RdC()
      s = 2._dp*dsqrt(1._dp-Rd(3))
      RdV = [ Rd(1)*s, Rd(2)*s, 1._dp-2._dp*Rd(3) ]
      return
      end function RdV
!-----------------------------------------------------------------------
      function RdQ()
      real(dp) :: RdQ(4)
      real(dp) :: Rd1(3),Rd2(3),s
      ! uniformly pick a random point on 4D unit sphere
      ! see paper
      ! "Choosing a point from the surface of a sphere"
      ! by George Marsaglia      
      ! cited in
      ! "Quaternions in molecular modeling"
      ! by Charles F. F. Karney
      Rd1 = RdC() ; Rd2 = RdC()
      s = dsqrt((1._dp-Rd1(3))/Rd2(3))
      RdQ = [ Rd1(1), Rd1(2), Rd2(1)*s, Rd2(2)*s ]
      return
      end function RdQ
!=======================================================================
      function Rot(axis,da)
      real(dp) :: Rot(3,3)
      integer ,intent(in) :: axis ! 1=x,2=y,3=z
      real(dp),intent(in) :: da
      real(dp) :: Rz(3,3)
      ! cc-rotation matrix along x,y, or z (axis point out)
      Rot = I3by3
      if (da == 0._dp) return
      Rz(1,1) = dcos(da) ; Rz(1,2) = -dsin(da) ; Rz(1,3) = 0._dp
      Rz(2,1) = dsin(da) ; Rz(2,2) =  dcos(da) ; Rz(2,3) = 0._dp
      Rz(3,1) = 0._dp    ; Rz(3,2) =  0._dp    ; Rz(3,3) = 1._dp
      Rot = cshift(cshift(Rz,shift=-axis,dim=2),shift=-axis,dim=1)
      return
      end function Rot
!!=======================================================================
      subroutine output(filename,NumAtoms,Ti,Ms)
      character(len=*),intent(in) :: filename
      integer,intent(in) :: NumAtoms
      real(dp),intent(in) :: Ti(:,:,:),Ms(:,:)
      integer :: iNP,ipt
      real(dp) :: pt(4)

      open(unit=50,file=filename,status="old", position="append",&
     &action="write")
      write(50,'(i0)') NumAtoms
      write(50,'(a20)') ' cube representation'
      do iNP=1,NP
      do ipt=1,NO-1 ! NO-1 to reserve space for dipole
      pt = matmul(Ti(:,:,iNP),Oxyz(:,ipt))
      write(50,'(a4,a1,3f16.6)') Oname(ipt),' ',pt(1:3)
      enddo
      pt(1:3) = 96.75_dp*Ms(:,iNP) + Ti(1:3,4,iNP)
      write(50,'(a4,a1,3f16.6)') Oname(10),' ',pt(1:3)
      enddo
      close(50)
      return
      endsubroutine output
!=======================================================================
      function acceptMove(deltaE)
      logical :: acceptMove
      real(dp),intent(in) :: deltaE
      real(dp) :: Rd,fac
      !---------------------------------------------------------------
      ! Decision making (metropolis scheme)
      !---------------------------------------------------------------
      ! delta E <= 0, always accept move
      ! delta E >  0, accept move with probability exp(-deltaE/kT)
      !---------------------------------------------------------------
      acceptMove = .True.
      if ( deltaE > 0._dp ) then
       call random_number(Rd)
       fac = exp( -deltaE/kT )
       if ( Rd > fac ) acceptMove = .False.
      endif
      return
      endfunction acceptMove
!=======================================================================
      function step(                                                    &
     &  imc,freq,current,minallow,maxallow,accept,propose,outrate)
      real(dp) :: step
      integer ,intent(inout) :: accept(:),propose(:)
      real(dp),intent(inout) :: outrate
      integer ,intent(in) :: imc,freq
      real(dp),intent(in) :: current,maxallow,minallow
      real(dp) :: rate
      ! modify step size based on accept rate
      step = current

      ! first few cycles ($burnin), update every $burnfreq. 
      ! After that update every $freq
      if ( imc+startfrom > burnin .AND. mod(imc,freq) /= 0 ) return
      if ( imc+startfrom <= burnin .AND. mod(imc,burnfreq) /= 0 ) return

      rate=real(sum(accept))/real(sum(propose))
      outrate = rate
      !write(100,*) 'accept rate: ',rate
      if ( rate > target_rate ) then
        if ( 1.05_dp*current <= maxallow) step = 1.05_dp*current
      elseif ( rate < target_rate ) then
        if ( 0.95_dp*current >= minallow) step = 0.95_dp*current
      endif
      propose = 0 ; accept = 0
      updated = .true.
      return
      endfunction step
!=======================================================================
      function MaxPt(direction,shape1)
      real(dp) :: MaxPt(3)
      real(dp),intent(in) :: direction(:),shape1(:,:)
      real(dp) :: dc(3),d(3),lamda_5,pt(3)
      ! find point of superellipsoid shape further in the direction given
      ! superellipsoid: x^6 + y^6 + z^6 = (a/2)^6
      ! gradient = (6x^5, 6y^5, 6z^5)
      ! direction = lamda*gradient   (Lagrange multiplier)
      ! x = (lamda*direction(1)/6)^(1/5) .....
      ! lamda = ( 6^(6/5) * (a/2)^6 / sum(direction^(6/5)) )^(5/6)
      ! lamda^(1/5) = (a/2) / sum(direction^(6/5))^(5/6)

      dc = matmul(transpose(shape1(1:3,1:3)),direction)
      d = abs(dc*0.166666667_dp)  ! abs(d/6)
      lamda_5 = 0.5_dp*a/(sum(d**1.2_dp)**0.166666667_dp)
      pt = sign([1._dp,1._dp,1._dp],dc) * lamda_5 * d**0.2_dp
      MaxPt = matmul(shape1(1:3,1:3),pt(1:3)) + shape1(1:3,4)
      return
      end function MaxPt
!=======================================================================
      function MaxDiff(direction,shape1,shape2)
      real(dp) :: MaxDiff(3)
      real(dp),intent(in) :: direction(:),shape1(:,:),shape2(:,:)
      ! Minkowski difference furtherest away in the direction
      MaxDiff = MaxPt(direction,shape1)-MaxPt(-direction,shape2)
      return
      end function MaxDiff
!=======================================================================
      function newSimplex(ix,simplex,direction)
      logical :: newSimplex
      real(dp),intent(inout) :: simplex(:,:),direction(:)
      integer ,intent(inout) :: ix
      real(dp) :: A(3),B(3),C(3),D(3),AB(3),AC(3),AD(3),AO(3)
      real(dp) :: nABC(3),nACD(3),nADB(3)

      newSimplex = .false. ! reset

      ! simplex is a line
      if ( ix == 2 ) then
        A = simplex(:,2) ; B = simplex(:,1)
        AO = -A ; AB = B - A
        ! get direction toward origin and perpendicular to line 
        direction = cross( cross(AB,AO), AB )
        return
      endif

      ! simplex is a triangle
      if ( ix == 3 ) then
        A = simplex(:,3) ; B = simplex(:,2) ; C = simplex(:,1)
        AO = -A ; AB = B - A ; AC = C - A
        nABC = cross(AB,AC) ! normal of triangle ABC
        if ( dot_product(cross(AB,nABC),AO) > 0._dp ) then
          ! origin is below edge AB
          ! simplex = [B,A]
          simplex=0._dp ; ix=2
          simplex(:,1) = B ; simplex(:,2) = A
          direction = cross( cross(AB,AO), AB )
          return
        elseif ( dot_product(cross(nABC,AC),AO) > 0._dp ) then
          ! origin is above edge AC
          ! simplex = [C,A]
          simplex=0._dp ; ix=2
          simplex(:,1) = C ; simplex(:,2) = A
          direction = cross( cross(AC,AO), AC )
          return
        elseif ( dot_product(nABC,AO) > 0._dp ) then
          ! origin is on +ve side of triangle
          ! simplex = [C,B,A]
          simplex=0._dp ; ix=3
          simplex(:,1) = C ; simplex(:,2) = B ; simplex(:,3) = A
          direction = nABC
          return
        else
          ! origin is on -ve side of triangle
          ! simplex = [B,C,A]
          simplex=0._dp ; ix=3
          simplex(:,1) = B ; simplex(:,2) = C ; simplex(:,3) = A
          direction = -nABC
          return
        endif
      endif
          
      ! simplex is a tetrahedron
      if ( ix == 4 ) then
        A = simplex(:,4) ; B = simplex(:,3)
        C = simplex(:,2) ; D = simplex(:,1)
        AO = -A ; AB = B - A ; AC = C - A ; AD = D - A
        nABC = cross(AB,AC) ! normal of triangle ABC
        nACD = cross(AC,AD) ! normal of triangle ACD
        nADB = cross(AD,AB) ! normal of triangle ADB
        if ( dot_product(nABC,AO) > 0._dp ) then
          ! origin is in front of triangle ABC
          if ( dot_product( cross(nABC,AC), AO ) > 0._dp ) then
            ! origin is close to edge AC
            ! simplex = [C,A]
            simplex=0._dp ; ix=2
            simplex(:,1) = C ; simplex(:,2) = A
            direction = cross( cross(AC,AO), AC )
            return
          elseif ( dot_product( cross(AB,nABC), AO ) > 0._dp ) then
            ! origin is close to edge AB
            ! simplex = [B,A]
            simplex=0._dp ; ix=2
            simplex(:,1) = B ; simplex(:,2) = A
            direction = cross( cross(AB,AO), AB )
            return
          else
            ! origin is in the face of triangle ABC
            ! simplex = [C,B,A]
            simplex=0._dp ; ix=3
            simplex(:,1) = C ; simplex(:,2) = B ; simplex(:,3) = A
            direction = nABC
            return
          endif
        elseif ( dot_product(nACD,AO) > 0._dp ) then
          ! origin is in front of triangle ACD
          if ( dot_product( cross(nACD,AD), AO ) > 0._dp ) then
            ! origin is close to edge AD
            ! simplex = [D,A]
            simplex=0._dp ; ix=2
            simplex(:,1) = D ; simplex(:,2) = A
            direction = cross( cross(AD,AO), AD )
            return
          elseif ( dot_product( cross(AC,nACD), AO ) > 0._dp ) then
            ! origin is close to edge AC
            ! simplex = [C,A]
            simplex=0._dp ; ix=2
            simplex(:,1) = C ; simplex(:,2) = A
            direction = cross( cross(AC,AO), AC )
            return
          else
            ! origin is in the face of triangle ACD
            ! simplex = [D,C,A]
            simplex=0._dp ; ix=3
            simplex(:,1) = D ; simplex(:,2) = C ; simplex(:,3) = A
            direction = nACD
            return
          endif
        elseif ( dot_product(nADB,AO) > 0._dp ) then
          ! origin is in front of triangle ADB
          if ( dot_product( cross(nADB,AB), AO ) > 0._dp ) then
            ! origin is close to edge AB
            ! simplex = [B,A]
            simplex=0._dp ; ix=2
            simplex(:,1) = B ; simplex(:,2) = A
            direction = cross( cross(AB,AO), AB )
            return
          elseif ( dot_product( cross(AD,nADB), AO ) > 0._dp ) then
            ! origin is close to edge AD
            ! simplex = [D,A]
            simplex=0._dp ; ix=2
            simplex(:,1) = D ; simplex(:,2) = A
            direction = cross( cross(AD,AO), AD )
            return
          else
            ! origin is in the face of triangle ADB
            ! simplex = [B,D,A]
            simplex=0._dp ; ix=3
            simplex(:,1) = B ; simplex(:,2) = D ; simplex(:,3) = A
            direction = nADB
            return
          endif
        else
          ! origin is within the tetrahedron = shapes overlap
          newSimplex = .true.
          return
        endif
      endif    
      end function newSimplex
!=======================================================================
      function GJK(shape1,shape2)
      logical :: GJK ! return true if overlap
      real(dp),intent(in) :: shape1(:,:),shape2(:,:)
      real(dp) :: newpt(3),simplex(3,4),direction(3)
      integer  :: ix,limit
      
      ix=0 ; simplex = 0._dp ; GJK = .False. !reset
      direction = shape2(1:3,4)-shape1(1:3,4)
      ix=1 ; simplex(:,ix) = MaxDiff(direction,shape1,shape2)
      if ( dot_product(simplex(:,1),direction) < 0._dp ) return 
      direction = -direction
      do limit=1,20
        newpt = MaxDiff(direction,shape1,shape2)
        ! exit if newpt does not enclose the origin 
        if ( dot_product(newpt,direction) < 0._dp ) return 
        ix=ix+1 ; simplex(:,ix) = newpt
        ! overlap if simplex enclose the origin 
        if ( newSimplex(ix,simplex,direction) ) then
          GJK = .True.
          return
        endif
      enddo
      return
      end function GJK
!=======================================================================
      function overlap(NPlist,Tnp)
      logical :: overlap ! return true if overlapped
      real(dp),intent(in) :: Tnp(:,:,:)
      integer ,intent(in) :: NPlist(:)
      real(dp) :: r(3),r1
      integer  :: iNP,jNP,i
      logical  :: dchk(NP,NP)
      overlap = .True.  
      dchk = .True. !reset

      ! quick COM check (spherical boundary)
      do i=1,size(NPlist) ; do jNP=1,NP
       iNP=NPlist(i)
       if ( iNP == jNP ) cycle
       r(:) = Tnp(1:3,4,iNP) - Tnp(1:3,4,jNP)
       r1 = sum(r(1:3)*r(1:3))
       if ( r1 <   minD2 ) return
       if ( r1 >=  maxD2 ) dchk(iNP,jNP) = .False.
      enddo ; enddo
!      print*,'~~~~~~~~~~~~~~~~~~~~ debug ~~~~~~~~~~~~~~~~~~~~~~~~~'

      ! more detailed check (based on GJK)
      do i=1,size(NPlist) ; do jNP=1,NP
       iNP=NPlist(i)
       if ( iNP == jNP ) cycle
       if ( dchk(iNP,jNP) ) then
        if ( GJK(Tnp(:,:,iNP),Tnp(:,:,jNP)) ) return 
       endif
      enddo ; enddo
      overlap = .False.

      return
      end function overlap
!=======================================================================
      subroutine update_neighbors(NPlist,Tnp,NotNeigh)
      logical ,intent(inout) :: NotNeigh(:,:)
      integer ,intent(in) :: NPlist(:)
      real(dp),intent(in) :: Tnp(:,:,:)
      integer  :: iNP,jNP,i
      real(dp) :: r(3),r1
      ! update neighbor list 
      do i=1,size(NPlist) ; do jNP=1,NP
       iNP=NPlist(i)
       NotNeigh(iNP,jNP) = .False.
       if ( iNP == jNP ) cycle
       r(:) = Tnp(1:3,4,iNP) - Tnp(1:3,4,jNP)
       r1 = sum(r(1:3)*r(1:3))
       if ( r1 > neighD2 ) NotNeigh(iNP,jNP) = .True.
      enddo ; enddo
      return
      end subroutine update_neighbors
!=======================================================================
      function surfacedist(Ti,Tj,output)
      logical :: surfacedist ! return true if overlap
      real(dp),intent(in) :: Ti(:,:),Tj(:,:)
      real(dp),intent(inout) :: output(NS)
      real(dp) :: x6,y6,z6,rfci(4),rfcl,rfcj(3),angx,angz,rshape
      integer :: ipt
      real(dp) :: temp(4)

      output = 0._dp
      do ipt=1,NS
       ! rfci is from center of neighbor to surface of iNP
       !rfci(:) = matmul(Ti,Sxyz(:,ipt)) - Tj(1:3,4)
       !rfci(:) = matmul(Ti,Sxyz(:,ipt)) - Tj(1:3,4)
       temp = matmul(Ti,Sxyz(:,ipt))
       temp(1:3) = temp(1:3) - Tj(1:3,4)
       rfci = temp
       rfcl = sqrt(sum(rfci(1:3)*rfci(1:3)))
       ! rfcj is normalized rfci in neighbor's coord
       rfcj(:) = norm(matmul(transpose(Tj(1:3,1:3)),rfci(1:3)))
       ! find theta phi, of rfcj (out from center of neighbor)
       angx = datan2(rfcj(2),rfcj(1))
       angz = dacos(max(-1.0_dp,min(1.0_dp,rfcj(3))))
       ! origin to cube surface
       x6 = (dsin(angz)*dcos(angx))**6._dp
       y6 = (dsin(angz)*dsin(angx))**6._dp
       z6 = (dcos(angz))**6._dp
       rshape = 0.5_dp*a / (x6+y6+z6)**0.166666667_dp
       output(ipt) = rfcl-rshape
       if ( output(ipt) <= 0._dp ) then
        surfacedist = .False.
        return
       endif
      enddo

      return
      end function surfacedist      
!=======================================================================
      subroutine cal_EvdW(NPlist,Tnp,NotNeigh,outputEvdW)
      use omp_lib

      real(dp),intent(inout) :: outputEvdW
      logical ,intent(in) :: NotNeigh(:,:)
      real(dp),intent(in) :: Tnp(:,:,:)
      integer ,intent(in) :: NPlist(:)
      real(dp) :: subEvdW
      real(dp) :: x(NS),SvdW,BvdW,vdWij,alph,r0,r(3),r1
      integer :: i,j,iNP,jNP,ipt,vpt1,vpt2,f(NS)
      real(dp) :: sumr6,pt1(4),v12(4)
      real(dp) :: temp(4)
      logical :: dummy

      ! omp variable
      integer, parameter :: num_threads = 2
      integer :: num_loops
      integer :: ppt
      integer :: istart
      integer :: iend
      integer :: thread_id

      !$ call omp_set_num_threads(num_threads)

      if (size(NPlist) /= NP .and. size(NPlist) /= 1) then
       print*,'can only calculate energy for all NPs or one NP'
       stop
      endif

      num_loops = size(NPlist)
      outputEvdW = 0._dp !#=:outputEvdW:shared


      do i=1,size(NPlist) 
        !$omp parallel default(none) &
        !$omp firstprivate(i,jNP,iNP,x,SvdW,sumr6,vpt1,pt1,vpt2,v12,BvdW,dummy) &
        !$omp firstprivate(vdWij) &
        !$omp firstprivate(istart,iend,ppt,thread_id, subEvdW) &
        !$omp private(temp) &
        !$omp shared(outputEvdW,NPlist,Tnp,NotNeigh,num_loops,Sarea,Vxyz) 

        ! omp loop set up
        !$omp do reduction(+:outputEvdW)
          do jNP=1,NP        !#=:jNP:private,NP:shared
          iNP=NPlist(i)    !#=:iNP:private
          if ( iNP == jNP ) cycle
          if ( NotNeigh(iNP,jNP) .OR. NotNeigh(jNP,iNP) ) cycle
          dummy = surfacedist(Tnp(:,:,iNP),Tnp(:,:,jNP),x) !#=:Tnp:shared, x:private
          !SvdW = 10._dp*sum(Sarea*& !#=:SvdW:private,Sarea:"damn, it is a data from the outter scope"
          !&(70._dp/(x+0.715_dp*a)**8._dp-0.0035_dp/(x+0.715_dp*a)**6._dp))
          SvdW = 290.*sum(Sarea/(x+0.715*a)**8._dp) 
         !SvdW = 10._dp*sum(Sarea*(70._dp/(x+0.715_dp*a)**8._dp-0.0035_dp/(x+0.715_dp*a)**6._dp))

          sumr6 = 0._dp !#=:sumr6:private
          do vpt1=1,NV  !#=:vpt1:private, NV:"damn, it's an alien"
           pt1 = matmul(Tnp(:,:,iNP),Vxyz(:,vpt1)) !#=:Vxyz:"alien",pt1:private
           do vpt2=1,NV !#=:vpt2:private
            v12 = matmul(Tnp(:,:,jNP),Vxyz(:,vpt2)) - pt1 !#=:v12:"private"
            sumr6 = sumr6 + 1._dp/dsqrt(sum(v12(1:3)*v12(1:3)))**6._dp
           enddo
          enddo
          BvdW = - sumr6 !#=:BvdW:private
          vdWij = KvdW*( SvdW + BvdW ) !#=:KvdW:"alien",vdWij:private
          if ( size(NPlist) == NP ) vdWij = vdWij*0.5_dp
          !subEvdW = subEvdW + vdWij
          outputEvdW = outputEvdW + vdWij
        enddo 
        !$omp end do
        !$omp end parallel
      enddo


      return
      end subroutine cal_EvdW
!=======================================================================
     subroutine cal_Emag(NPlist,Tnp,Ms,outputEdd,outputEa,outputEz,NotNeigh)
      use omp_lib

      integer, intent(in) :: NPlist(:)
      logical ,intent(in) :: NotNeigh(:,:)
      real(dp),intent(in) :: Tnp(:,:,:),Ms(:,:)   
      real(dp),intent(inout) :: outputEdd,outputEa,outputEz
      real(dp) :: subEdd
      integer  :: iNP,i,jNP
      real(dp) :: Edd,Mc(3),r(3),dotr,M1(3),M2(3),r1,r3,r5
      real(dp) :: pt1(3)
      real(dp) :: temp(4)
      integer :: vpt1,vpt2

      ! omp variable
      integer, parameter :: num_threads = 2
      integer :: num_loops 
      integer :: ppt
      integer :: istart
      integer :: iend
      integer :: thread_id
      num_loops = size(NPlist)

      !$ call omp_set_num_threads(num_threads)

      if (size(NPlist) /= NP .and. size(NPlist) /= 1) then
       print*,'can only calculate energy for all NPs or one NP'
       stop
      endif

      outputEdd = 0._dp

      ! print *,"thread_id = ",thread_id," istart = ", istart, " iend = ", iend
      ! print *, "ppt = ", ppt, "num_loops = ", num_loops, "size(NPlist) = ", size(NPlist), " NPlist = ", NPlist

      !!$ do i=istart,iend !#=: i:private,istart:private,iend:private
      do i=1,size(NPlist)  ! do loop will be run multiple times
        !$omp parallel default(none) &
        !$omp firstprivate(i,jNP,iNP,vpt1,pt1,vpt2,r,dotr,r1,r3,r5) &
        !$omp firstprivate(M1,M2,Edd) &
        !$omp firstprivate(istart,iend,ppt,thread_id,subEdd) &
        !$omp private(temp) &
        !$omp shared(outputEdd,NPlist,Tnp,NotNeigh,Vxyz,num_loops,Ms)

        ! omp loop set up
        !$omp do reduction(+:outputEdd)
           do jNP=1,NP !#=: jNP:private, NP:const
          iNP=NPlist(i) !#=: iNP:private, NPlist:shared
          if ( iNP == jNP ) cycle 
          if ( NotNeigh(iNP,jNP) .OR. NotNeigh(jNP,iNP) ) cycle !#=:NotNeigh:shared
          M1(:) = Ms(:,iNP) ; M2(:) = Ms(:,jNP) !#=:
          do vpt1=1,NV
            temp = matmul(Tnp(:,:,iNP),Vxyz(:,vpt1))
            pt1 = temp(1:3)
            ! print *,"size(pt1) = ",size(pt1), "size(Vxyz) = ",size(Vxyz(:,vpt1))
            do vpt2=1,NV
             temp = matmul(Tnp(:,:,jNP),Vxyz(:,vpt2))
             r = temp(1:3) - pt1
             dotr = sum(r*r)
             if (dotr > 0._dp) then         
               r1 = dsqrt(dotr) ; r3 = r1*r1*r1 ; r5 = r3*r1*r1
               Edd = Kdd*(sum(M1*M2)/r3-3._dp*sum(M1*r)*sum(M2*r)/r5)/NV ! 1/27
               if ( size(NPlist) == NP ) Edd = Edd*0.5_dp
             else
               Edd = 0._dp
             endif
             ! print *,"id = ", thread_id, "Edd = ", Edd
             ! subEdd = subEdd + Edd/NV
             outputEdd = outputEdd + Edd/NV
            enddo
          enddo
          ! print *,"id = ", thread_id,"jNP = ", jNP 
        enddo 
        !$omp end do
        ! print *,"id = ", thread_id, "subEdd = ", subEdd
        !!$omp critical
        !outputEdd = outputEdd + subEdd
        !! print *,"id = ", thread_id, "Edd = ", outputEdd
        !!$omp end critical
        !$omp end parallel
        !print *,"outputEdd = ", outputEdd
      enddo


      outputEa = 0._dp
      do i=1,size(NPlist)
       iNP = NPlist(i)
       Mc(:) = matmul(transpose(Tnp(1:3,1:3,iNP)),Ms(:,iNP))
       outputEa = outputEa + Ka*sum((Mc*cshift(Mc,1))**2)
      enddo

      outputEz = 0._dp
      do i=1,size(NPlist)
       iNP = NPlist(i)
       outputEz = outputEz + Kz*sum(Ms(:,iNP)*Hs)
      enddo
      return
      end subroutine cal_Emag     
!=======================================================================
      subroutine importdata()
!.....................................................................
      call system_clock (count=clock_start)
!.....................................................................
      !---------------------------------------------------------------
      ! Import NP coordinates: xyz1(x/y/z, Bead number, NP number)
      if ( startfrom == 0 ) then
       open (unit=1,file='coordinate.xyz') 
      else
       write(infile,'(a9,i0,a4)') 'output/fr',startfrom,'.xyz'
       open (unit=1,file=infile)
      endif
      read (1,*) i
      if ( NA /= i ) then
       print'(a20,i0)','Error: Change NA to ',i
       stop
      endif
      read (1,*)
      print'(a26,i0,a8)','Importing coordinates of ',NP,' NPs ...'
      NPxyz = 0._dp
      do i=1,NP ; do j=1,NB
      read (1,*) junk,(NPxyz(k,j,i),k=1,3)
      enddo ; enddo
      close(1)
      NPxyz(4,:,:) = 1._dp ! for homogeneous coordinate
      !--------------------------------------------------------------
      ! Cube mesh: SXYZ(x/y/z, point number)
      open (unit=2,file='CubeMesh_sorted.txt')
      read (2,*) i
      if ( NS /= i ) then
       print'(a20,i0)','Error: Change NS to ',i
       stop
      endif          
      read (2,*) 
      print'(a11,i0,a15)','Importing ',NS,' surface points'
      Sxyz = 0._dp
      do i=1,NS
      read (2,*) (Sxyz(k,i),k=1,3),Sarea(i)
      enddo
      close(2)
      Sxyz = Sxyz*a ; Sarea = Sarea*a*a
      Sxyz(4,:) = 1._dp ! for homogeneous coordinate
      !--------------------------------------------------------------
      ! Cube output representation
      open (unit=3,file='CubeRep_10.xyz')
      read (3,*) i
      if ( NO /= i ) then
       print'(a20,i0)','Error: Change NO to ',i
       stop
      endif
      read (3,*) 
      print'(a11,i0,a20)','Importing ',NO,' Cube representation'
      Oxyz = 0._dp
      do i=1,NO
      read (3,*) junk,(Oxyz(k,i),k=1,3)      
      enddo
      close(3)
      Oxyz(4,:) = 1._dp ! for homogeneous coordinate
      !--------------------------------------------------------------
      ! Volume mesh
      open (unit=4, file='Cube_volume.xyz')
      read (4,*) i
      if (NV /= i) then
       print*,'Error: Change NV to ', i
       stop
      endif
      print'(a11,i0,a20)','Importing ', NV,' volume points'
      read (4,*)
      Vxyz = 0._dp
      do i=1,NV
      read(4,*) junk,(Vxyz(k,i), k=1,3)
      enddo
      close(4)
      Vxyz=Vxyz*a
      vxyz(4,:) = 1._dp ! for homogeneous coordinate 
!.....................................................................
      call system_clock (count=clock_stop)
      print*,'took',real(clock_stop-clock_start)/real(clock_rate),'sec'
!.....................................................................
      end subroutine importdata
!=======================================================================


!=======================================================================
      ! From External sources
!=======================================================================
      subroutine shuffle(a)
      !----------------------------------------------------------
      ! Knuth Shuffle 
      ! http://rosettacode.org/wiki/Knuth_shuffle#Fortran
      !----------------------------------------------------------
      integer, intent(inout) :: a(:)
      integer :: i, randpos, temp
      real :: r
      do i = size(a), 2, -1
        call random_number(r)
        randpos = int(r * i) + 1
        temp = a(randpos)
        a(randpos) = a(i)
        a(i) = temp
      enddo
      end subroutine shuffle
!=======================================================================
      subroutine init_random_seed()
      !----------------------------------------------------------
      ! http://gcc.gnu.org/onlinedocs/gfortran/
      !                 RANDOM_005fSEED.html#RANDOM_005fSEED
      !----------------------------------------------------------
      implicit none
      integer, allocatable :: seed(:)
      integer :: i, n, un, istat, dt(8), pid, t(2), s
      integer(8) :: count, tms
      
      call random_seed(size = n)
      allocate(seed(n))
      ! First try if the OS provides a random number generator
      open(newunit=un, file="/dev/urandom", access="stream",            &
     & form="unformatted", action="read", status="old", iostat=istat)
      if (istat == 0) then
         read(un) seed
         close(un)
      else
         ! Fallback to XOR:ing the current time and pid. The PID is
         ! useful in case one launches multiple instances of the same
         ! program in parallel.
         call system_clock(count)
         if (count /= 0) then
            t = transfer(count, t)
         else
            call date_and_time(values=dt)
            tms = (dt(1) - 1970) * 365_8 * 24 * 60 * 60 * 1000          &
     &           + dt(2) * 31_8 * 24 * 60 * 60 * 1000                   &
     &           + dt(3) * 24 * 60 * 60 * 60 * 1000                     &
     &           + dt(5) * 60 * 60 * 1000                               &
     &           + dt(6) * 60 * 1000 + dt(7) * 1000                     &
     &           + dt(8)
            t = transfer(tms, t)
         end if
         s = ieor(t(1), t(2))
         pid = getpid() + 1099279 ! Add a prime
         s = ieor(s, pid)
         if (n >= 3) then
            seed(1) = t(1) + 36269
            seed(2) = t(2) + 72551
            seed(3) = pid
            if (n > 3) then
               seed(4:) = s + 37 * [ (i, i = 0, n - 4) ]
            end if
         else
            seed = s + 37 * [ (i, i = 0, n - 1 ) ]
         end if
      end if
      call random_seed(put=seed)
      end subroutine init_random_seed
!=======================================================================        
      end module procedures
!=======================================================================        

!!=======================================================================
!      function RandVecOnCone(ax,rad)
!      real(dp) :: RandVecOnCone(3)
!      real(dp),intent(in) :: ax(:),rad
!      real(dp) :: cosang,rand(2),sumsq,faxis,frota,frotb,fnorm
!      
!      cosang = dcos(max(-1.0_dp,min(1.0_dp,rad)))
!
!      sumsq = 9._dp ! reset
!      do while (sumsq > 1._dp)
!        call random_number(rand)
!        rand = 1._dp-2._dp*rand
!        sumsq = sum(rand*rand)
!      enddo
!
!      fnorm = dsqrt( (1._dp - cosang*cosang) / sumsq)
!      rand = rand*fnorm
!
!      faxis = sign(1._dp,ax(3))
!      frota = sum(rand*ax(1:2))
!      frotb = cosang - frota/(1._dp+faxis*ax(3))
!
!      RandVecOnCone(1:2) = rand + ax(1:2)*frotb
!      RandVecOnCone(3) = cosang*ax(3) - faxis*frota
!      end function RandVecOnCone
!!=======================================================================
