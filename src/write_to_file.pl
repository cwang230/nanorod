use Ref::Util qw(is_arrayref);

sub write_to_file {
	
		my ($input) = @_;

		my $array_rf   = $input->{array};         # input array...
		my $write_file = $input->{file};          # file to write... 

		open (my $write_fh, ">".$write_file);

		my $current_data = undef;
		foreach my $current_item (@{$array_rf}) {
				
				$current_data = is_arrayref($current_item)?
												join " ", @{$current_item} :
												$current_item;

				# if it is a reference...
				print $write_fh $current_data."\n";

		}

		close($write_fh)

}
