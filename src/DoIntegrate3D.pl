#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use List::MoreUtils 'pairwise'; # pairwise addition...
use List::Util qw(sum);


#==============================================================
# read array data...
# parameters:
# left : Array for structure on the left...
# right : Array for structure on the left...
# height : height of the structrue == length of single array...
# area : number of sample points in a single layer...
#==============================================================

my $left = [];
my $right = [];
my $height = 0;
my $area = 0;

my @total = ();

my $disk1 = $left->[0];
my $disk2 = [];
# calculate the interaction between two layers...
foreach my $layer_id (0..$height-1) {

	# add one more layer...
	push @total, 0;

	# use coordinates from different layer...
	$disk2 = $right->[$layer_id];
	
	foreach my $i (@{$disk1}) {
		foreach my $j (@{$disk2}) {

			# calculate dd interactoin...
			# memoization is used...
			$total[$layer_id] += dd($i,$j); 

		}
	}
}

# After the above loop, we should have an array @total 
# which has all types of dd interactions between two disks.
# Make use of those types of dd interactions to construct
# the overall dd interaction...

# ...


#===========
# subroutine
#===========

sub dd {
	my ($p1, $p2) = @_;
	
	# set default value to empty array if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return 1.0/dist($p1,$p2);
}

sub dist {
	my ($p1, $p2) = @_;
	
	# set default value to empty array if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return 
		sqrt 
		sum
		pairwise { ($a-$b)**2 } @{$p1}, @{$p2};
}
