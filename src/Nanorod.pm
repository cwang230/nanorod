package Nanorod;

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                      # floor function...
use List::MoreUtils 'pairwise'; # pairwise addition...
use Data::Dumper;               # show data hierarchy...
use List::Util qw(sum reduce);
use List::Flatten;
use File::Slurp;                # enable file slurp during input...
use Math::MatrixReal;
use Math::Trig;
use Ref::Util qw(is_arrayref);                  # test if object is an array reference...
use Math::Random::NormalDistribution;
use MCE::Loop;

use base 'Exporter';

our @EXPORT = qw(
				shell_coordinates_gen_test
				rotate_y
				rotate_axis
				rotate_axis_2
				rotate_mce
				translate_mce
				dd
				vdw
				dist
				cylinder_coordinates_gen
				rec_coordinates_gen
				shell_coordinates_gen
				shell_coordinates_gen_boring
				shell_coordinates_normal_gen
				coordinates_move
				translate
				write_to_file
				charge_distro_local
				charge_distro_local_duplicate
				eta_charge
);


#====================
# enable test feature
#====================

sub shell_coordinates_gen_test {

	# set up input...
	my ($input) = @_;

	# parameter setup...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;                        # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my $N = $input->{points}     // 100;
	my @rectangle = ();  # coordinates of rectangle grid...

	# need an array to store coordinates for all 
	my @coordinates = ();
	my $angle = 0;

	foreach my $id (0..$N-1) {
		$angle = 2*pi;
		# $angle = rand(1)*2*pi;
		push @coordinates, [
			pairwise { $a+$b } 
				@{[
					$R * cos($angle),
					$R * sin($angle),
					$H/2, 
				]},
				@{$C}
		];
	}

	# output...
	push @{ $input->{shell} }, @coordinates;

}

#===========
# subroutine
#===========

sub rotate_y {
	
	my ($input) = @_;

	my $angle = $input->{angle} // 0.0;
	my $vector = [$input->{vector}] // [[0.0, 0.0, 0.0]];

	my $vector_mat = Math::MatrixReal->new_from_cols($vector);

	my $rot_mat = Math::MatrixReal->new_from_rows(
		[
			[cos($angle), 0.0, sin($angle)],
			[0.0,         1.0, 0.0        ],
			[-sin($angle),0.0, cos($angle)],
		]
	);

	my $new_vector_mat = $rot_mat->multiply($vector_mat);
	return (~$new_vector_mat)->[0]->[0];
}

sub rotate_axis {
	
				my ($input) = @_;

				my $angle  = $input->{angle}    // 0.0;
				my $vector = $input->{vector} // [[0.0, 0.0, 0.0]];
				my $axis   = $input->{axis}     // "x";

				my $vector_mat = Math::MatrixReal->new_from_cols([$$vector]);

				my $matrix_choice = {

								x =>				[
																				[1.0, 0.0        , 0.0         ],
																				[0.0, cos($angle), -sin($angle)],
																				[0.0, sin($angle), cos($angle) ],
																],

								y =>			[
																				[cos($angle), 0.0, sin($angle)],
																				[0.0,         1.0, 0.0        ],
																				[-sin($angle),0.0, cos($angle)],
																],
																
								z =>			[
																				[cos($angle), -sin($angle), 0.0],
																				[sin($angle), cos($angle) , 0.0],
																				[0.0        , 0.0         , 1.0],
																],

				};

				my $rot_mat = Math::MatrixReal->new_from_rows( $matrix_choice->{$axis} );

				my $new_vector_mat = $rot_mat->multiply($vector_mat);

				# use left-dereference to change the value alread sitting in memory...
				$$vector = (~$new_vector_mat)->[0]->[0];

				return 0;
}

sub rotate_axis_2 {
	
				my ($input) = @_;

				my $angle  = $input->{angle}    // 0.0;
				my $vector = $input->{vector}   // [[0.0, 0.0, 0.0]];
				my $axis   = $input->{axis}     // "x";

				my $vector_mat = Math::MatrixReal->new_from_cols([$vector]);

				my $matrix_choice = {

								x =>				[
																				[1.0, 0.0        , 0.0         ],
																				[0.0, cos($angle), -sin($angle)],
																				[0.0, sin($angle), cos($angle) ],
																],

								y =>			[
																				[cos($angle), 0.0, sin($angle)],
																				[0.0,         1.0, 0.0        ],
																				[-sin($angle),0.0, cos($angle)],
																],
																
								z =>			[
																				[cos($angle), -sin($angle), 0.0],
																				[sin($angle), cos($angle) , 0.0],
																				[0.0        , 0.0         , 1.0],
																],

				};

				my $rot_mat = Math::MatrixReal->new_from_rows( $matrix_choice->{$axis} );

				my $new_vector_mat = $rot_mat->multiply($vector_mat);

				# use left-dereference to change the value already sitting in memory...
				$vector = (~$new_vector_mat)->[0]->[0];

				return $vector;
}

sub translate_mce {

				my ($input) = @_;

				my $coords = $input->{coords};     # the array of coordinates...
				my $path_vector = $input->{path_vector} // [];

				my $new_coords = [mce_loop {
								my ($mce,$chunk_ref,$chunk_id) = @_;
								
								my @pos_mce = ();
								foreach my $each_point ($chunk_ref->@*) {
												translate({
																coordinates => \$each_point,
																path_vector => $path_vector,
												});

												push @pos_mce, $each_point;
								} 
								$mce->gather(@pos_mce);
				} $coords->@*];

				return $new_coords;
}

sub rotate_mce {

				my ($input) = @_;

				my $coords = $input->{coords};     # the array of coordinates...
				my $angle  = $input->{angle};
				my $axis   = $input->{axis} // "x";
				my $path_vector = $input->{path_vector} // [];
				my $trans_if = $input->{trans_if} // 0; 
				
				
				my $new_coords = [mce_loop {
								my ($mce,$chunk_ref,$chunk_id) = @_;

								my @pos_mce = ();
								foreach my $each_point ($chunk_ref->@*) {
												; my $TRANS = $trans_if;

												; if ($TRANS == 1) {
												translate({
																coordinates => \$each_point,
																path_vector => $path_vector,
												});
												; }

												# say Dumper $each_point;
												$each_point = rotate_axis_2({
																angle => $angle,
																vector=> $each_point,
																axis  => $axis,
												});

												; if ($TRANS == 1) {
												translate({
																coordinates => \$each_point,
																path_vector => [map {-$_} $path_vector],
												});
												; }

												push @pos_mce, $each_point;
								}
								$mce->gather(@pos_mce);
				} $coords->@*];

				return $new_coords;
}


sub dd {
	my ($p1, $p2, $idl) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];
	$idl //= 0.0; # inverse debye length...

	return exp(-1*$idl*dist($p1,$p2)) 
				 * 1.0 
				 / dist($p1,$p2);
}

# vdw interaction...
sub vdw {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return dist($p1,$p2)**(-6);
}

sub dist {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return 
		sqrt 
		sum
		pairwise { ($a-$b)**2 } @{$p1}, @{$p2};
}

sub cylinder_coordinates_gen {

	# set up the input...
	my ($input) = @_;     # input with hash...

	# parameter setup...
	my $d = $input->{grid_size}   // 1.4;       # the grid_size between points...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;       # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my @rectangle = ();  # coordinates of rectangle grid...

	# figure out properties of cylinders...
	my $n = floor(2*$R/$d); # max number of points along radius... 
	my $m = floor($H/$d); # number of layers... 

	# need an array to store coordinates for all 
	# candidate coordinates horizontally...
	my @coordinates = ();

	# at least this many number of points are needed...
	foreach my $index (0..$n) {  
		push @coordinates, (-$n/2+$index)*$d;
	}

	# coordinates vertically...
	my @heights = ();
	foreach my $index (0..$m) {
		push @heights, (-$m/2+$index)*$d;
	}

	# say Dumper $C;

	# pick up coordinates from @coordinates 
	# and form array rectangle...
	foreach my $height (@heights) {
		foreach my $row (@coordinates) {
			push @rectangle, [           # each array reference here represents
			                             # coordinate of a point...
				map {
					$_**2 + $row**2 < $R**2 ?   # filter those which are not in the circle...

					[
						pairwise { $a+$b }       # pairwise addition from two arrays below...
						@{[$_,$row,$height]},  # dereference version:sandwich version of array...
						@{$C}
					]        :

					[ ]                      # otherwise, return an empty array reference...
				} 
				@coordinates

			];
		}
	}

	# print information about system for analysis...
#	say "Number of layers => ", $m+1;

	push @{ $input->{cylinder} }, 
			 grep { scalar @{ $_ } > 0 } # remove empty list...
       flat @rectangle;

	return 0;
}

# coordinates for generating rectangle objects with hallow...

sub make_rec {
				# ({:x,y,z,step,center,coords})

				my ($input) = @_;

				my $dim = {
								x => $input->{x} // 420,        # unit => nanometer
								y => $input->{y} // 420,
								z => $input->{z} // 830,
				};

				my $step = $input->{step} // 10;         # unit => nanometer
				my $center = $input->{center} // [0,0,0];

				# unit => nanometer
				my $num = {
								x => floor($dim->{x}/$step),
								y => floor($dim->{y}/$step),
								z => floor($dim->{z}/$step),
				};

				my $coords = {
								x => [],
								y => [],
								z => [],
				};

				# coords => x
				push $coords->{x}->@*, map {$step * ($_ - $num->{x}/2)} (0..$num->{x});
				push $coords->{y}->@*, map {$step * ($_ - $num->{y}/2)} (0..$num->{y});
				push $coords->{z}->@*, map {$step * ($_ - $num->{z}/2)} (0..$num->{z});

				# translate the coordinates...
				# output dimension => 3*$num->{x}*$num->{y}*$num->{z}...
				my $rectangle = $input->{coords};
				foreach my $z ($coords->{z}->@*) {
								foreach my $y ($coords->{y}->@*) {
												push @{$rectangle},
																map {[pairwise {$a+$b} [$_,$y,$z]->@*,$center->@*]}
																				$coords->{x}->@*
												;
								}
				}

				return 0;
}

sub rec_coordinates_gen {
				# ({:x,y,z,step,center,coords})

				my ($input) = @_;

				my $size_hollow = $input->{hollow} // 88.0;

				# the rectangular dimension...
				my $dim_rec = {                               
								x => $input->{x} // 420,        # unit => nanometer
								y => $input->{y} // 420,                           
								z => $input->{z} // 830,
				};
				 
				# the hollow dimension...
				my $dim_hollow = {                               
								x => $input->{x} - 2*$size_hollow // 420 - 78*2, # unit => nanometer
								y => $input->{y} - 2*$size_hollow // 420 - 78*2,                    
								z => $input->{z}                // 830,
				};

				my $center = $input->{center};
				my $step   = $input->{step};

				my $coords = [];
				make_rec({
								x     =>$dim_rec->{x},
								y     =>$dim_rec->{y},
								z     =>$dim_rec->{z},
								center=>$center, 
								coords=>$coords,
								step  =>$step,
				});

				# say Dumper $coords;

				push $input->{coords}->@*, grep {!(
												(-1*$dim_hollow->{x}/2 < $_->[0] && $_->[0] < $dim_hollow->{x}/2) 
												&&
												(-1*$dim_hollow->{y}/2 + $center->[1] < $_->[1] && 
													$_->[1] < $dim_hollow->{y}/2 + $center->[1]) 
												)} $coords->@* 
								  ;

				# say Dumper $input->{coords};

				return 0;
}

sub shell_coordinates_gen_boring {

				# set up input...
				my ($input) = @_;

				# parameter setup...
				my $d = $input->{grid_size}   // 1.4;       # the grid_size between points...
				my $t = $input->{angle}       // 0.02*2*pi;
				my $R = $input->{radius}      // 17.0/2.0;
				my $H = $input->{height}      // 22.0;
				my $s = $input->{separation}  // 8.8;                        # separation between two rods...
				my $C = $input->{center}      // [0.0, -1*$s/2.0-$R ,0.0];


				# coordinates vertically...
				my $m = floor($H/$d); # number of layers... 
				my @heights = ();
				foreach my $index (0..$m) {
								push @heights, (-$m/2+$index)*$d;
				}

				# coordinates horizontally...
				my $slices = floor(2*pi/$t); # number of slices... 
				my @angles = ();
				foreach my $index (0..$slices) {
								push @angles, (-$slices/2+$index)*$t;
				}

				# need an array to store coordinates for all...
				my @coordinates = ();
				foreach my $layer (@heights) {
								foreach my $angle (@angles) {
												
												push @{ $input->{shell} }, [
																pairwise { $a+$b } 
																				@{[
																								$R * cos($angle),
																								$R * sin($angle),
																								$layer, 
																				]},
																				@{$C}
												]

								}
				}


}

sub shell_coordinates_gen {

	# set up input...
	my ($input) = @_;

	# parameter setup...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;                        # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my $N = $input->{points}     // 100;
	my @rectangle = ();  # coordinates of rectangle grid...

	# need an array to store coordinates for all 
	my @coordinates = ();
	my $angle = 0;

	foreach my $id (0..$N-1) {
		$angle = rand(1)*2*pi;
		push @coordinates, [
			pairwise { $a+$b } 
				@{[
					$R * cos($angle),
					$R * sin($angle),
					$H*rand(1) - $H/2, 
				]},
				@{$C}
		];
	}

}

sub shell_coordinates_normal_gen {

				# set up input...
				my ($input) = @_;

				# parameter setup...
				my $R = $input->{radius}     // 17.0/2.0;
				my $H = $input->{height}     // 22.0;
				my $s = $input->{separation} // 8.8;                        # separation between two rods...
				my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
				my $N = $input->{points}     // 100;
				my @rectangle = ();  # coordinates of rectangle grid...

				# need an array to store coordinates for all 
				my @coordinates = ();
				my $angle = 0;
				my $z_height = 0;   # for input to a distribution...     

				foreach my $id (0..$N-1) {

				# parameters for random position...
				$angle = rand(1)*2*pi;
				$z_height = $H*rand(1) - $H/2;

				push @coordinates, [
				pairwise { $a+$b } 
								@{[
													$R * cos($angle),
													$R * sin($angle),
													$H * sin($z_height), 
								]},
								@{$C}
				];

				}

				# output...
				push @{ $input->{shell} }, @coordinates;

				return 0;

}

sub coordinates_move {
				
				my ($input) = @_;
				
				my $current_coordinates = $input->{coordinates} // [];    # 
				my $path_vector = $input->{path_vector} // [0,0,0];

				# move by vector...
				foreach my $point (@{ $current_coordinates }) { # it will work, the reference is dereferenced...
								
								$point = [
												pairwise { $a+$b } 
																@{ $point }, @{ $path_vector }
								];

				}

				return 0;
}

sub translate {
				
				my ($input) = @_;
				
				my $current_coordinates = $input->{coordinates} // [];    # 
				my $path_vector = $input->{path_vector} // [0,0,0];

				# move by vector... use left-dereference to change the value alread sitting in memory...
				$$current_coordinates = [  # [CAUTION] disconnects the relation with original reference...      
								pairwise { $a+$b } 
												@{ $$current_coordinates }, @{ $path_vector }
				];

				return 0;
}

sub write_to_file {
	
		my ($input) = @_;

		my $array_rf   = $input->{array};         # input array...
		my $write_file = $input->{file};          # file to write... 

		open (my $write_fh, ">".$write_file);

		my $current_data = undef;
		foreach my $current_item (@{$array_rf}) {
				
				$current_data = is_arrayref($current_item)?
												join " ", @{$current_item} :
												$current_item;

				# if it is a reference...
				print $write_fh $current_data."\n";

		}

		close($write_fh);

		return 0;
}

sub charge_distro_local {
				
				my ($input) = @_;
				
				my $point      = $input->{point} ;           # pass in the reference to reference...
				                                             # for left-dereference...
				my $axis       = $input->{axis} // "z";
				my $max_angle  = $input->{max_angle} // pi;
				my $max_height = $input->{max_height} // 0.0;
				my $center     = $input->{center};

				# step 1: 
				translate({
								coordinates => $point,
								path_vector => [map {-$_} @{$center}],
				});

				# figure out where to move...
				my $angle = rand(1) * $max_angle-$max_angle/2;
				my $end_height = $max_height*rand(1)-$max_height/2.0;
				my $path_vector = [0.0,0.0,$end_height];

				rotate_axis({
								angle=>$angle,
								vector=>$point,
								axis=>$axis});
				
				# move it...
				translate({
								coordinates => $point,
								path_vector => $path_vector,
				});

				# step -1: 
				translate({
								coordinates => $point,
								path_vector => $center,
				});

				return 0;
}

sub charge_distro_local_duplicate {
				
				my ($input) = @_;
				
				my $point      = $input->{point} ;           # pass in the reference to reference...
				                                             # for left-dereference...
				my $axis       = $input->{axis} // "z";
				my $max_angle  = $input->{max_angle} // pi;
				my $max_height = $input->{max_height} // 0.0;
				my $center     = $input->{center};

				# figure out where to move...
				my $angle = rand(1) * $max_angle-$max_angle/2;
				my $end_height = $max_height*rand(1)-$max_height/2.0;
				my $path_vector = [0.0,0.0,$end_height];

				# step 1: 
				translate({
								coordinates => $point,
								path_vector => [map {-$_} @{$center}],
				});

				rotate_axis({
								angle=>$angle,
								vector=>$point,
								axis=>$axis});
				
				# move it...
				translate({
								coordinates => $point,
								path_vector => $path_vector,
				});

				# step -1: 
				translate({
								coordinates => $point,
								path_vector => $center,
				});

				return $$point;
}

sub eta_charge {

				my $distance = shift @_ // 0.0;
				my $height = shift @_   // 1.0;
				
				return 1 - cos(pi*$distance/$height);
				
}

sub eta_charge_exp {

				my $distance = shift @_ // 0.0;
				my $height = shift @_   // 1.0;
				
				return cos(pi*$distance/$height);
				
}

1; # Magic true value required at end of module
