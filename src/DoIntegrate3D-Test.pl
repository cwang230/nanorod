#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use List::MoreUtils 'pairwise'; # pairwise addition...
use List::Util qw(sum reduce);
use File::Slurp;                # enable file slurp during input...
use Data::Dumper;               # show data hierarchy...

#==============================================================
# read array data...
# parameters:
# left : Array for structure on the left...
# right : Array for structure on the left...
# height : height of the structrue == length of single array...
# area : number of sample points in a single layer...
#==============================================================

#============
# read inputs
#============

# first cylinder...
my @cylinder_1 = map {[ split ' ',$_ ]} read_file('coords_left.txt');

# second cylinder...
my @cylinder_2 = map {[ split ' ',$_ ]} read_file('coords_right.txt');

# set up the number of layers for each cylinder...
my $layers_per_cylinder = 16;

# number of points per layer...
my $points_per_layer = (scalar @cylinder_1) / $layers_per_cylinder;
say "number of points per layer => ", $points_per_layer;

#=====================================
# test what's returned by read_file...
#=====================================
# 
# say scalar @cylinder_1 / $layers_per_cylinder;
# say join '->', @{$cylinder_1[0]};


#=============
# the algoritm
#=============

# find the first layer in cylinder_1 and record it exclusively...
my $base_layer = [ @cylinder_1[0..$points_per_layer-1] ];

# compute each layer-layer interaction between $base-layer and 
# each one in @cylinder_2...
my $current_layer = 0;
my $current_start_point_id = 0;
my $current_end_point_id = 0;
my $interaction_per_layer = 0;
my @layer_layer_interaction = ();
while ($current_layer < $layers_per_cylinder) {

	# loop over each points in that layer...
	$current_start_point_id = 
			$points_per_layer * $current_layer;
	$current_end_point_id = 
			$current_start_point_id + $points_per_layer - 1;

	# clear the interaction buffer...
	$interaction_per_layer = 0;

	foreach my $point_base_layer (@{$base_layer}) {

		foreach my $current_point_id (
			$current_start_point_id
		..$current_end_point_id) {

			# interaction between two points...
			$interaction_per_layer += 
				dd($point_base_layer,
					 $cylinder_2[$current_point_id]
				);

		}

	}

	# record interaction per layer...
	push @layer_layer_interaction, $interaction_per_layer;

	# update the layer id...
	$current_layer += 1;

}

#========================
# enlarge the interaction
#========================

my $multiple = $layers_per_cylinder;
foreach my $energy_per_layer (@layer_layer_interaction) {

	# enlarge the interaction...
	$energy_per_layer *= $multiple;
	
	# decrease the multiple number...
	$multiple -= 1;

}

# output the total energy...
my $total_energy = reduce {$a+$b} @layer_layer_interaction;
my $constant = 0.0000338376;
# my $constant = 4.2*10**(-6);
$total_energy *= $constant;
say $total_energy;

# say Dumper(\@layer_layer_interaction);
# say scalar @layer_layer_interaction;
# say Dumper($base_layer->[0]);
# say Dumper($cylinder_2[111]);
# say Dumper($cylinder_2[112]);
  
# my @left = ([0, 0, 0],);
# my @right = ([1, 1, 1],);
# 
# say dd($left[0],$right[0]);
# 
# my $ar = [[1,1,1],[2,2,2],[3,3,3],[4,4,4]];
# my $a01 = [@{$ar}[0..1]];
# 
# say Dumper $a01;


#====================
# close files handler
#====================
# close($cylinder_1);

#===========
# subroutine
#===========

sub dd {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	# I haven't included the unit charge per point...
##	return exp(-1*0.52464*dist($p1,$p2))   
	return exp(-1*1.95221*dist($p1,$p2)) 
				 * 1.0 
				 / dist($p1,$p2);
}

sub dist {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return 
		sqrt 
		sum
		pairwise { ($a-$b)**2 } @{$p1}, @{$p2};
}
