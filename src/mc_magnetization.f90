      include 'procedures.f90'

      program MonteCarlo
      use procedures
      use omp_lib
      implicit none
      real(dp) :: com(3), rotation(3,3), rr(3)
      real(dp) :: Rd4Array(NP,4)
      real(dp) :: Rd4Temp(4)
      logical :: flag = .True.
      integer :: num_threads = 112

      integer :: i0,i1 = 0
      integer :: n_tot = NP
      integer :: s_len 
      integer :: n_sec
      integer :: r_up = 0, r_down=0

      s_len = num_threads
      n_sec = n_tot/s_len

      call init_random_seed()
      !!print *,"if nested: ",omp_get_nested()
      call system_clock (count_rate=clock_rate)
      print*,'---------------------------------------------------------'

      call importdata()
      call init_random_seed()

      print*,'---------------------------------------------------------'
      print*,'Parameters:'
      print*
      print*,' a, rm, neigh cutoff'
      print*,a,rm,dsqrt(neighD2)
      print'(a16,f7.1,a2)','Temperature = ',Temp,' K'      
      print'(a8,3f7.1,a4)','Hs = [',Hs,' ] G'
      print*
      open(unit=102,file='stepsize.txt',access='append')
      I4by4 = 0._dp ; do i=1,4; I4by4(i,i) = 1._dp ; enddo 
      I3by3 = I4by4(1:3,1:3)

      do iNP=1,NP
       T1np(  :,:,iNP) = I4by4
       T1np(1:3,1,iNP) = norm(NPxyz(1:3, 3,iNP)-NPxyz(1:3, 2,iNP))
       T1np(1:3,2,iNP) = norm(NPxyz(1:3, 5,iNP)-NPxyz(1:3, 4,iNP))
       T1np(1:3,3,iNP) = norm(NPxyz(1:3, 7,iNP)-NPxyz(1:3, 6,iNP))
       T1np(1:3,4,iNP) = NPxyz(1:3,1,iNP)
       ! magnetic dipole

!!       if ( startfrom == 0 ) then
!!        Ms1(1:3,iNP) = Rdv() ! randomized dipole
!!       else
       Ms1 (1:3,iNP) = norm(NPxyz(1:3,10,iNP)-NPxyz(1:3, 1,iNP))
!!       endif
      enddo
      print*,'checking initial structure (exit if overlap found)'
      call system_clock (count=clock_start)
      call system_clock (count=clock_stop)
      print*,'took',real(clock_stop-clock_start)/real(clock_rate),'sec'
      print*,'updating neighor list'
      call system_clock (count=clock_start)
      call update_neighbors(NPs,T1np,NotNeigh1)
      call system_clock (count=clock_stop)
      print*,'took',real(clock_stop-clock_start)/real(clock_rate),'sec'
!.....................................................................
      Esys = 0._dp ! reset
      print*,'calculating Emag'
      call system_clock (count=clock_start)
      !print *,"Esys(2) = ", Esys(2)
      !print *,"size(NPs) = ", size(NPs)
      call cal_Emag(NPs,T1np,Ms1,Esys(2),Esys(3),Esys(4),NotNeigh1)
      !print *,"Esys(2) = ", Esys(2)
      call system_clock (count=clock_stop)
      print*,'took',real(clock_stop-clock_start)/real(clock_rate),'sec'
!.....................................................................
      print*,'calculating EvdW'
      call system_clock (count=clock_start)
      call cal_EvdW(NPs,T1np,NotNeigh1,Esys(1))
      call system_clock (count=clock_stop)
      print*,'took',real(clock_stop-clock_start)/real(clock_rate),'sec'

      print*,' KvdW, Kdd, Ka Kz'
      print*,KvdW,Kdd,Ka,Kz
      print*,' EvdW, Edd, Ea, Ez'
      print*,Esys/NP
      print*,' avg tilt (dipole)'
      print*,dacos(sum(Ms1(3,:))/NP)*180._dp/PI



      print*,'---------------------------------------------------------'
      print'(a8,i0,a30)','running ',run,' MC cycles, output every cycle'
      propose  = 0 ; accept  = 0

      MCcycle: do imc=1,run ! Run through MC cycles

      ! change magnetic field every 20 runs...
      if (mod(imc, 20) == 0) then
        Hs(1) = Hs(1) - 10
      endif

      !$  call omp_set_num_threads(num_threads)
      call system_clock (count=clock_start)

      call init_random_seed()
!!      call shuffle(NPs) ! randomized order of NPs in list
!!
!!      do i = 1,NP
!!        call random_number(Rd4Temp)
!!        Rd4Array(i,:) = 2._dp*Rd4Temp(:)-1._dp
!!      end do
!!
!!
!!      !$omp parallel default(none) &
!!      !$omp private(i1,iNP,NotNeigh1,NotNeigh2,Eall)     &     
!!      !$omp shared(NPs,ds,da,Ms1,T1np,T0np) &
!!      !$omp shared(n_tot,s_len,n_sec,r_up,r_down) &
!!      !$omp firstprivate(Rd4Array,T2np,flag)    & 
!!      !$omp reduction(+:propose,accept)
!!
!!      ! length = n_sec + 1; +1 is important here
!!      MainLoop: do i0 = 0,n_sec 
!!
!!      !$omp barrier
!!      !$omp master
!!			r_up = i0*s_len+1    
!!			if (i0 < n_sec) then
!!				r_down = (i0+1)*s_len
!!			else
!!				r_down = n_tot
!!			endif
!!
!!      T0np = T1np
!!      !$omp end master
!!      !$barrier
!!
!!      !$omp do
!!      thisNP: do i1 = r_up, r_down
!!!!    print*,'~~~~~~~~~~~~~~~~~~~~ debug ~~~~~~~~~~~~~~~~~~~~~~~~~'
!!
!!      iNP = NPs(i1) ! Number of current NP
!!      propose = propose + 1
!!      T2np = T0np;
!!      Eall = 0._dp
!!      NotNeigh1 = .False.
!!      NotNeigh2 = .False.
!!      ! rotation dof...?
!!      T2np(1:3,4,iNP) = T2np(1:3,4,iNP) + Rd4Array(iNP,1:3)*ds
!!      T2np(1:3,1:3,iNP)=matmul(qRot(RdV(),Rd4Array(iNP,4)*da),T2np(1:3,1:3,iNP))
!!      if ( overlap([iNP],T2np) ) cycle thisNP
!!!!      if ( overlap([iNP],T2np) ) flag = .False.
!!!!      call overlap([iNP],T2np,flag) 
!!      call update_neighbors([iNP],T0np,NotNeigh1)
!!!!      !! NotNeigh2 = NotNeigh1 !! no need
!!      call cal_EvdW([iNP],T0np,NotNeigh1,Eall(1,1))
!!      call                                                              &
!!     &cal_Emag([iNP],T0np,Ms1,Eall(2,1),Eall(3,1),Eall(4,1),NotNeigh1)
!!      call update_neighbors([iNP],T2np,NotNeigh2)
!!      call cal_EvdW([iNP],T2np,NotNeigh2,Eall(1,2))
!!      call                                                              &
!!     &cal_Emag([iNP],T2np,Ms1,Eall(2,2),Eall(3,2),Eall(4,2),NotNeigh1)
!!!!      if ( flag .and. acceptMove(sum(Eall(:,2))-sum(Eall(:,1))) ) then
!!      if ( acceptMove(sum(Eall(:,2))-sum(Eall(:,1))) ) then
!!        accept = accept + 1
!!        T1np(:,:,iNP) = T2np(:,:,iNP)
!!        !!NotNeigh1 = NotNeigh2 !! no need
!!      endif
!!      enddo thisNP
!!      !$omp end do
!!      enddo MainLoop
!!      !$omp end parallel
  
      do k=1,10
      Ms0 = Ms1
      call shuffle(NPs) ! randomized order of NPs in list
      !$ call omp_set_num_threads(num_threads)
      !! dipole part has not been included
      !$omp parallel default(none) &
      !$omp private(i,iNP)     &     
      !$omp private (Eall,Ms2,NotNeigh0) &
      !$omp shared(NPs,T1np,Ms1,k,Ms0)
      !$omp do
      Dipole: do i=1,NP
        iNP=NPs(i)
        Ms2 = Ms0 ; Eall = 0._dp ! reset
        Ms2(:,iNP) = Rdv()
        !NotNeigh0 = .False.
        call update_neighbors([iNP],T1np,NotNeigh0)
        call                                                              &
       &cal_Emag([iNP],T1np,Ms0,Eall(2,1),Eall(3,1),Eall(4,1),NotNeigh0)
      call                                                              &
     &cal_Emag([iNP],T1np,Ms2,Eall(2,2),Eall(3,2),Eall(4,2),NotNeigh0)
      if ( acceptMove(sum(Eall(:,2))-sum(Eall(:,1))) ) then
        Ms1(:,iNP) = Ms2(:,iNP)
      endif
      enddo Dipole
      !$omp end do
      !$omp end parallel
      enddo

      call system_clock (count=clock_stop)
      print*,'took',real(clock_stop-clock_start)/real(clock_rate),'sec'

      call output('frs.xyz',NO*NP,T1np,Ms1)
!!      ! optimize step size based on succss rate
!!      accrate = real(sum(accept(:)))/real(sum(propose(:)))
!!      updated = .false.
!!      ds  = step(imc,dsf,ds,dsn,dsM,accept(1:1),propose(1:1),rate(1))
!!      da  = step(imc,daf,da,dan,daM,accept(2:2),propose(2:2),rate(2))
!!      if ( updated ) then
!!       write(102,*) imc+startfrom,rate(1),ds,rate(2),da
!!       flush(102)
!!      endif

      enddo MCcycle ! completed all cycles
      close(102) ! stepsize output
      end program MonteCarlo
