#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use Carp;
use Getopt::Long;


# Set up the command line interface...
my $field;
GetOptions (
	"field=i" => \$field,
) or die("Error in command line arguments\n");
say "field= " . $field;


my $current_frame= Field2Frame(field=>$field);
say "frame= " . $current_frame;


# make subroutine...
sub Field2Frame {
	croak "Missing value for a named arg" if @_ % 2; # @_ = 2 => @_ % 2 -> 0 => if false;...

	my %ags = @_; # convert it to hash format...  
	my $field = $ags{field} // 0;   # set default value for field...  
	my $base_field = $ags{base_field} // 2000;   # set default value for field...  
	my $step = $ags{step} // 20;       # set default value for step...  
	my $change = $ags{change} // -10;  # set default value for change...  
	my $current_frame= $ags{current_frame} // 0;      # set default value for frame...

	$current_frame = ($field-$base_field)*$step/$change; 

	return $current_frame;
}
