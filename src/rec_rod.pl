#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use Data::Dumper;
use POSIX;
use List::MoreUtils qw(pairwise);

#====================================================|
# property:                                          |
#                                                    |
# * A rod with rectanglular shape                    |
# * Specs                                            |
# 				* Center => (0,0,0)                            |
# 				* x      => (-420/2,420/2)     # 420/2 = 210   |
# 				* y      => (-420/2,420/2)     # 420/2 = 210   |
# 				* z      => (-830/2,830/2)     # 830/2 = 415   |
# * Parameters                                       |
# 				* Size of grid => 10 nm                        |
# 								* Number of points in x axis => 420/10=42; |
# 								* Number of points in y axis => 420/10=42; |
# 								* Number of points in z axis => 830/10=83; |
# 				* Charge associated on points                  |
#====================================================|

sub make_rec {
				# ({:x,y,z,step,center,coords})

				my ($input) = @_;

				my $dim = {                               
								x => $input->{x} // 420/2,        # unit => nanometer
								y => $input->{y} // 420/2,                           
								z => $input->{z} // 830/2,
				};

				my $step = $input->{step} // 10;         # unit => nanometer 
				my $center = $input->{center} // [0,0,0];

				# unit => nanometer
				my $num = {
								x => floor($dim->{x}/$step),
								y => floor($dim->{y}/$step),
								z => floor($dim->{z}/$step),
				};

				my $coords = {
								x => [],
								y => [],
								z => [],
				};

				# coords => x
				push $coords->{x}->@*, map {$step * ($_ - $num->{x}/2)} (0..$num->{x});
				push $coords->{y}->@*, map {$step * ($_ - $num->{y}/2)} (0..$num->{y});
				push $coords->{z}->@*, map {$step * ($_ - $num->{z}/2)} (0..$num->{z});

				# translate the coordinates...
				# output dimension => 3*$num->{x}*$num->{y}*$num->{z}...
				my $rectangle = $input->{coords};
				foreach my $z ($coords->{z}->@*) {
								foreach my $y ($coords->{y}->@*) {
												push @{$rectangle}, 
																map {[pairwise {$a+$b} [$_,$y,$z]->@*,$center->@*]} 
																				$coords->{x}->@*
												;
								}
				}

				return 0;
}

my $coords = [];
my $dim = {
				x => 420/2-78*2,
				y => 420/2-78*2,
				z => 830/2,
};
make_rec({coords=>$coords});

say scalar $coords->@*;

$coords = [ grep {
												(-1*$dim->{x} < $_->[0] && $_->[0] < $dim->{x}) 
												||
												(-1*$dim->{y} < $_->[1] && $_->[1] < $dim->{y}) 
												} $coords->@* 
								  ];
# say Dumper $coords;

say scalar $coords->@*;
