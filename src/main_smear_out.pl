#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                           # floor function...                                 
use List::MoreUtils 'pairwise';      # pairwise addition...                              
use Data::Dumper;                    # show data hierarchy...                            
use List::Util qw(sum reduce);                                                          
use List::Flatten;                                                                      
use File::Slurp;                     # enable file slurp during input...                 
use Math::MatrixReal;                                                                   
use Math::Trig;                                                                         
use Ref::Util qw(is_arrayref);       # test if object is an array reference...           
use Nanorod;

BEGIN {$| = 1}

my $experiment = "in";

# set up the initial distance for initial coordinates of cylinders and shells...

my $init_distance = 5.0;
my $final_distance = 6.0;

#-----------------------------------
# set up cylidner, shell coordinates
#-----------------------------------
                                         
# parameters...

my $ligand_size = 4.00;  # nm
my $separation_shell = $init_distance - 2*$ligand_size;
my $separation = $init_distance;
my $radius = 17.0/2.0;  # nm
my $height = 22.0;      # nm
my $grid_size = 1.4;    # nm
my %center = (
				left  => [0.0, -1*$separation/2.0-$radius, 0.0],
				right => [0.0,  1*$separation/2.0+$radius, 0.0],
);
my $num_rand_shell = 8*12;    # total number of random points...           

# generate <shell> and <cylinder>...

my $cylinder_left = [];
my $cylinder_right = [];

cylinder_coordinates_gen({
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{left},
				cylinder => $cylinder_left,
});

cylinder_coordinates_gen({
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{right},
				cylinder => $cylinder_right,
});

my $shell_left = [];
my $shell_right = [];
my $d_angle = 0.01*2*pi;

shell_coordinates_gen_boring({
				grid_size => $grid_size,
				angle  => $d_angle,
				radius => $radius + $ligand_size,
				height => $height,
				separation => $separation_shell,
				center => $center{left},
				shell => $shell_left,
});

shell_coordinates_gen_boring({
				grid_size => $grid_size,
				angle  => $d_angle,
				radius => $radius + $ligand_size,
				height => $height,
				separation => $separation_shell,
				center => $center{right},
				shell => $shell_right,
});

#******************************
# duplicate points for shell...
#******************************


; my $LOCAL_DIST = 0;
; if ($LOCAL_DIST == 1) {

my $angle = pi/20;
my $end_height = 2.0;

# right shell...
my @shell_right_copy = @{$shell_right};
foreach my $point (@shell_right_copy) {

				# duplicate 12 points around each $point...
				foreach (1..12) {
								push @{$shell_right}, charge_distro_local_duplicate({
												point => \$point,                     
												axis  => "z",                        
												max_angle => $angle,                 
												max_height => $end_height,          
												center     => $center{right},
								});
				}

}

# left shell...
my @shell_left_copy = @{$shell_left};
foreach my $point (@shell_left_copy) {

				# duplicate 12 points around each $point...
				foreach (1..12) {
								push @{$shell_left}, charge_distro_local_duplicate({
												point => \$point,                     
												axis  => "z",                        
												max_angle => $angle,                 
												max_height => $end_height,          
												center     => $center{left},
								});
				}

}

;}

#******************************************************* 
# REMOVE POINTS IN BETWEEN
#
# goal:     remove points from shell on the right.
# criteria: the distance between points on the right and 
#           the center on the left should be larger than
#           the $radius + $ligand_size.
#******************************************************* 

; my $SHELL = 1;
; if ($SHELL == 99) {                                # don't use this...
my $criteria_radius = $radius + $ligand_size;
$shell_left = [ grep 
																{ dist([@{$_}[0..1]],[@{$center{right}}[0..1]]) > $criteria_radius } 
															@{ $shell_left } ];

$shell_right = [ grep 
																{ dist([@{$_}[0..1]],[@{$center{left}}[0..1]]) > $criteria_radius } 
															@{ $shell_right } ];

;}

# set up output folder...
my $dir = "../results-out/";
my $file_export = "";

# output the configuration...
write_to_file({array=>$shell_right,file=>$dir."shell_right.txt"});
write_to_file({array=>$shell_left,file=>$dir."shell_left.txt"});
write_to_file({array=>$cylinder_right,file=>$dir."cylinder_right.txt"});
write_to_file({array=>$cylinder_left,file=>$dir."cylinder_left.txt"});

# code mute...
; my $DEBUG = 1;

;if ($DEBUG == 1) {  # __DEBUG__

#--------------------------
# set up specs for solution
#--------------------------
                                              
my %idls = (         # inverse Debye length...
				in => 1.38042,   # nm^-1             
				out => 0.52464,  # nm^-1             
);                                       
                                         
my $idl = $idls{$experiment};
my $hmk = 3e-19; # Hamaker constant => J
                                         
# electric constants...
my $epsilon0 = 8.85e-21;   # F nm^-1...
my %epsilon = (            # dielectric constant...
				in  => 80,               # unit.? 
				out => 80,
);
my $pts_per_cylinder = scalar @{ $cylinder_left };
my %sigmas = (                       # charges for each point...
				in  => 1.6e-19, 
				out => 1.6e-19,
);
my $sigma = $sigmas{$experiment};    
my $joule2eV = 6.242e18;   # conversion between joule to eV...
my %epss = (               # the constant part of electrostatic...
				in  => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{in})**(-1),
				out => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{out})**(-1),
);
my $eps = $epss{$experiment};

#***************************************
# extra work for the boring distribution
#***************************************

my $num_slices = floor(2*pi/$d_angle);
my $weighted_num_charges = $num_slices * 
																											sum( map {eta_charge($_->[2],$height)} @{ $shell_right } );
my $weighted_charge_density = $num_rand_shell / $weighted_num_charges;

#----------
# compute  
#----------

# set up the sampling distances...
my @distances = map {$_/10.0} ($init_distance*10)..($final_distance*10);

# set up the sampling angles...
my @angles = map {$_*pi/180}    # convert it from degree to radius...
					         grep {$_%5==0}    # choose angle by every 5 degree from 0 degree...
					         (-90..90);        # set up the sampling angle...

my %path_vector = (
				left  => [],
				right => [],
);

# keep track of interaction...
my @interaction_vdw = ();
my @interaction_dd = ();
my @interaction = ();
my $dv = pi*$radius**2*$height/$pts_per_cylinder;
my $index = 0;                   # layer id...

# set up two cylinders, two shells...
my @cylinder_1 = ();
my @cylinder_2 = ();
my @shell_1 = ();
my @shell_2 = ();

# for the copy of points on shell2...
my $p2_cp = [];

foreach my $distance (@distances) {

				# get the path vector...
				$path_vector{left} = [0, -($distance-$init_distance)/2.0, 0];
				$path_vector{right} = [0, ($distance-$init_distance)/2.0, 0];

				# separate cylinders more...
				# CAUTION, IT MIGHT NOT BE CORRECT...
				coordinates_move({
								coordinates=>$cylinder_left,
								path_vector=>$path_vector{left},
				});
				coordinates_move({
								coordinates=>$cylinder_right,
								path_vector=>$path_vector{right},
				});
				coordinates_move({
								coordinates=>$shell_left,
								path_vector=>$path_vector{left},
				});
				coordinates_move({
								coordinates=>$shell_right,
								path_vector=>$path_vector{right},
				});

				say "compute ".$distance."...";

				# get a local copy of coordinates...
				@cylinder_1 = @{ $cylinder_left };
				@cylinder_2 = @{ $cylinder_right };
				@shell_1 = @{ $shell_left };
				@shell_2 = @{ $shell_right };

				$index = 0;

				foreach my $angle (@angles) {

								# pick up one cylinder...
								@cylinder_2 = @{ $cylinder_right };

								# rotate the cylinder...
								foreach my $pt (@cylinder_2) {
												$pt = rotate_y({angle=>$angle,vector=>$pt});
								}

								# pick up one shell...
								@shell_2 = @{ $shell_right };

								# rotate one shell...
								foreach my $pt (@shell_2) {
												$pt = rotate_y({angle=>$angle,vector=>$pt});
								}

								# filter the overlap...
								; if ($SHELL == 1) {

								# copy for filter...
								@shell_1 = @{ $shell_left };
								
								my $criteria_radius = $radius + $ligand_size;
								@shell_1 = grep 
																								{ dist([@{$_}[0..1]],[@{$center{right}}[0..1]]) > $criteria_radius } 
																							@shell_1;

								@shell_2 = grep 
																								{ dist([@{$_}[0..1]],[@{$center{left}}[0..1]]) > $criteria_radius } 
																							@shell_2;

								;}

								# compute the interaction_vdw...
								$interaction_vdw[$index] = 0;
								foreach my $p1 (@cylinder_1) {
												foreach my $p2 (@cylinder_2) {

																# interaction_vdw calculated...
																$interaction_vdw[$index] += 
																	$joule2eV * (
																		(-1)*pi**(-2)*$hmk*vdw($p1,$p2)          # vdw interaction_vdw...
																	) * ($dv**2);

												}
								}

								# compute the interaction_dd...
								$interaction_dd[$index] = 0;
								foreach my $p1 (@shell_1) {
												foreach my $p2 (@shell_2) {

																# copy of $p2 for figuring out charge...
																$p2_cp = [@{ $p2 }];  # ref to array of numbers...
																rotate_axis({
																				angle => -$angle,
																				vector=> \$p2_cp,
																				axis  => "y",
																});

																# interaction_dd calculated...
																$interaction_dd[$index] += 
																				$joule2eV                    * 
																				$eps*dd($p1,$p2,$idl)        *     # dd interaction_vdw...
																				$weighted_charge_density * eta_charge($p1->[2],$height) *
																				$weighted_charge_density * eta_charge($p2_cp->[2],$height);

												}
								}

								# combine results...
								$interaction[$index] = 
												$interaction_vdw[$index] + $interaction_dd[$index];

								say $interaction_vdw[$index]." --> ".$interaction_dd[$index];

								# increment index...
								$index += 1;

				}

				# set up file name...
				$file_export = "interaction_".$experiment."-".$distance."nm.txt";

				# output the value of interaction...
				write_to_file({array=>\@interaction,file=>$dir.$file_export});
				write_to_file({array=>\@interaction_vdw,file=>$dir."vdw_".$file_export});
				write_to_file({array=>\@interaction_dd,file=>$dir."ele_".$file_export});

}

;} # __DEBUG__

END {$| = 0}


