#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                      # floor function...
use List::MoreUtils 'pairwise'; # pairwise addition...
use Data::Dumper;               # show data hierarchy...
use List::Util qw(sum reduce);
use List::Flatten;
use File::Slurp;                # enable file slurp during input...
use Math::MatrixReal;
use Math::Trig;

#=========================================
# (:-> 
#  d:distance between two grid points,
#  R:radius of the circle,
#  C:center of the circle,
#  H:heght of the circle,
#  s:separation between two rods,
# )^<setup>
# -->
# n:number of intervals between two points horizontally
# = (2*R/d)._<floor>
# m:number of intervals between two points vertically
# = (H/d)._<floor>
#=========================================

#==========
# main code
#==========

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Generate coordinates for two cylinders...
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

my $cylinder_left = [];
my $cylinder_right = [];
my $separation = 20.8;
my $radius = 17.0/2.0;
my $height = 22.0;
my $distance = 1.4;
my $center_left = [0.0, -1*$separation/2.0-$radius, 0.0];
my $center_right = [0.0, 1*$separation/2.0+$radius, 0.0];

cylinder_coordinates_gen( {
		distance => $distance,
		radius => $radius,
		height => $height,
		separation => $separation,
		center => $center_left,
		cylinder => $cylinder_left,
	}
);

cylinder_coordinates_gen( {
		distance => $distance,
		radius => $radius,
		height => $height,
		separation => $separation,
		center => $center_right,
		cylinder => $cylinder_right,
	}
);

# say Dumper $cylinder_left;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Compute interactions between two cylinders...
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#==============================================================
# read array data...
# parameters:
# left : Array for structure on the left...
# right : Array for structure on the right...
# height : height of the structrue == length of single array...
# area : number of sample points in a single layer...
#==============================================================

# set up two cylinders...
my @cylinder_1 = @{ $cylinder_left };
my @cylinder_2 = @{ $cylinder_right };

# rotate them if necessary...
my $theta = 0.0; 
foreach my $p1 (@{$cylinder_right}) {
	# rotate_y({angle=>pi/4.0,vector=>$cylinder_right->[$p1]});
	$p1 = rotate_y({angle=>pi/10.0,vector=>$p1});
}

# say Dumper $cylinder_right;

open(my $fh, ">coords_right_test.txt");
# open(my $fh, ">coords_left.txt");
foreach my $L1 (@{$cylinder_right}) {
			(say $fh join ' ', @$L1) if @$L1 != 0;
}
close($fh);

# keep track of interaction...
my $interaction = 0;

# compute...
# foreach my $p1 (@cylinder_1) {
# 	foreach my $p2 (@cylinder_2) {
# 		$interaction += dd($p1,$p2);
# 	}
# }


#===========
# subroutine
#===========

sub rotate_y {
	
	my ($input) = @_;

	my $angle = $input->{angle} // 0.0;
	my $vector = [$input->{vector}] // [[0.0, 0.0, 0.0]];

	my $vector_mat = Math::MatrixReal->new_from_cols($vector);

	my $rot_mat = Math::MatrixReal->new_from_rows(
		[
			[cos($angle), 0.0, sin($angle)],
			[0.0,         1.0, 0.0        ],
			[-sin($angle),0.0, cos($angle)],
		]
	);

	my $new_vector_mat = $rot_mat->multiply($vector_mat);
	return (~$new_vector_mat)->[0]->[0];
}

# my $another = Math::MatrixReal
# 			        ->new_from_rows($cylinder_left)
# 							->[0];            # get the array reference back...
# 
# say Dumper $another;

sub dd {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	# I haven't included the unit charge per point...
##	return exp(-1*0.52464*dist($p1,$p2))   
	return exp(-1*1.95221*dist($p1,$p2)) 
				 * 1.0 
				 / dist($p1,$p2);
}

sub dist {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return 
		sqrt 
		sum
		pairwise { ($a-$b)**2 } @{$p1}, @{$p2};
}

sub cylinder_coordinates_gen {

	# set up the input...
	my ($input) = @_;     # input with hash...

	# parameter setup...
	my $d = $input->{distance}   // 1.4;       # the distance between points...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;       # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my @rectangle = ();  # coordinates of rectangle grid...

	# figure out properties of cylinders...
	my $n = floor(2*$R/$d); # max number of points along radius... 
	my $m = floor($H/$d); # number of layers... 

	# need an array to store coordinates for all 
	# candidate coordinates horizontally...
	my @coordinates = ();

	# at least this many number of points are needed...
	foreach my $index (0..$n) {  
		push @coordinates, (-$n/2+$index)*$d;
	}

	# coordinates vertically...
	my @heights = ();
	foreach my $index (0..$m) {
		push @heights, (-$m/2+$index)*$d;
	}

	# say Dumper $C;

	# pick up coordinates from @coordinates 
	# and form array rectangle...
	foreach my $height (@heights) {
		foreach my $row (@coordinates) {
			push @rectangle, [           # each array reference here represents
			                             # coordinate of a point...
				map {
					$_**2 + $row**2 < $R**2 ?   # filter those which are not in the circle...

					[
						pairwise {$a+$b}       # pairwise addition from two arrays below...
						@{[$_,$row,$height]},  # dereference version:sandwich version of array...
						@{$C}
					]        :

					[ ]                      # otherwise, return an empty array reference...
				} 
				@coordinates

			];
		}
	}

	# print information about system for analysis...
#	say "Number of layers => ", $m+1;

	push @{ $input->{cylinder} }, 
			 grep { scalar @{ $_ } > 0 } # remove empty list...
       flat @rectangle;

	return 0;
}
