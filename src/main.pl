#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                      # floor function...
use List::MoreUtils 'pairwise'; # pairwise addition...
use Data::Dumper;               # show data hierarchy...
use List::Util qw(sum reduce);
use List::Flatten;
use File::Slurp;                # enable file slurp during input...
use Math::MatrixReal;
use Math::Trig;
use Ref::Util qw(is_arrayref);                  # test if object is an array reference...

BEGIN {$| = 1}

my $experiment = "in";

# set up the initial distance for initial coordinates of cylinders and shells...

my $init_distance = 8.3;
my $final_distance = 9.9;

# set up cylidner, shell coordinates...
                                         
my $ligand_size = 4.10;  # nm
my $separation_shell = $init_distance - 2*$ligand_size;
my $separation = $init_distance;
my $radius = 17.0/2.0;  # nm
my $height = 22.0;      # nm
my $grid_size = 1.4;    # nm
my %center = (
				left  => [0.0, -1*$separation/2.0-$radius, 0.0],
				right => [0.0,  1*$separation/2.0+$radius, 0.0],
);
my $num_rand_shell = 1000;

my $cylinder_left = [];
my $cylinder_right = [];
my $shell_left = [];
my $shell_right = [];

shell_coordinates_gen({
				radius => $radius + $ligand_size,
				height => $height,
				separation => $separation_shell,
				center => $center{left},
				points => $num_rand_shell,
				shell => $shell_left,
});

shell_coordinates_gen({
				radius => $radius + $ligand_size,
				height => $height,
				separation => $separation_shell,
				center => $center{right},
				points => $num_rand_shell,
				shell => $shell_right,
});

cylinder_coordinates_gen({
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{left},
				cylinder => $cylinder_left,
});

cylinder_coordinates_gen({
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{right},
				cylinder => $cylinder_right,
});

# set up specs for solution...
                                              
my %idls = (         # inverse Debye length...
				in => 1.38042,   # nm^-1             
				out => 0.52464,  # nm^-1             
);                                       
                                         
my $idl = $idls{$experiment};
my $hmk = 3e-19; # Hamaker constant => J
                                         
# electric constants...
my $epsilon0 = 8.85e-21;   # F nm^-1...
my %epsilon = (            # dielectric constant...
				in  => 80,               # unit.? 
				out => 80,
);
my $pts_per_cylinder = scalar @{ $cylinder_left };
my %sigmas = (                       # charges for each point...
				in  => 2.5 * 3*11*12*1.6e-19 / $num_rand_shell, 
				out => 0,
);
my $sigma = $sigmas{$experiment};    
my $joule2eV = 6.242e18;   # conversion between joule to eV...
my %epss = (               # the constant part of electrostatic...
				in  => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{in})**(-1),
				out => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{out})**(-1),
);
my $eps = $epss{$experiment};

# compute...

# set up the sampling distances...
my @distances = map {$_/10.0} ($init_distance*10)..($final_distance*10);

# set up the sampling angles...
my @angles = map {$_*pi/180}    # convert it from degree to radius...
					grep {$_%5==0}             # choose angle by every 5 degree from 0 degree...
					(-90..90);                 # set up the sampling angle...

my %path_vector = (
				left  => [],
				right => [],
);


# program control...
my $dir = "../results3/";
my $file_export = "";

# keep track of interaction...
my @interaction_vdw = ();
my @interaction_dd = ();
my @interaction = ();
my $dv = pi*$radius**2*$height/$pts_per_cylinder;
my $index = 0;                   # layer id...

# set up two cylinders, two shells...
my @cylinder_1 = ();
my @cylinder_2 = ();
my @shell_1 = ();
my @shell_2 = ();

foreach my $distance (@distances) {

				# get the path vector...
				$path_vector{left} = [0, -($distance-$init_distance)/2.0, 0];
				$path_vector{right} = [0, ($distance-$init_distance)/2.0, 0];

				# separate cylinders more...
				coordinates_move({
								coordinates=>$cylinder_left,
								path_vector=>$path_vector{left},
				});
				coordinates_move({
								coordinates=>$cylinder_right,
								path_vector=>$path_vector{right},
				});
				coordinates_move({
								coordinates=>$shell_left,
								path_vector=>$path_vector{left},
				});
				coordinates_move({
								coordinates=>$shell_right,
								path_vector=>$path_vector{right},
				});

				say "compute ".$distance."...";

				# get a local copy of coordinates...
				@cylinder_1 = @{ $cylinder_left };
				@cylinder_2 = @{ $cylinder_right };
				@shell_1 = @{ $shell_left };
				@shell_2 = @{ $shell_right };

				$index = 0;

				foreach my $angle (@angles) {
								
								# pick up one cylinder...
								@cylinder_2 = @{ $cylinder_right };

								# rotate the cylinder...
								foreach my $pt (@cylinder_2) {
												$pt = rotate_y({angle=>$angle,vector=>$pt});
								}

								# pick up one shell...
								@shell_2 = @{ $shell_right };

								# rotate one shell...
								foreach my $pt (@shell_2) {
												$pt = rotate_y({angle=>$angle,vector=>$pt});
								}

								# compute the interaction_vdw...
								$interaction_vdw[$index] = 0;
								foreach my $p1 (@cylinder_1) {
												foreach my $p2 (@cylinder_2) {

																# interaction_vdw calculated...
																$interaction_vdw[$index] += 
																	$joule2eV * (
																		(-1)*pi**(-2)*$hmk*vdw($p1,$p2)          # vdw interaction_vdw...
																	) * ($dv**2);

												}
								}

								# compute the interaction_dd...
								$interaction_dd[$index] = 0;
								foreach my $p1 (@shell_1) {
												foreach my $p2 (@shell_2) {

																# interaction_dd calculated...
																$interaction_dd[$index] += 
																	$joule2eV * (
																		$eps*dd($p1,$p2,$idl)                   # dd interaction_vdw...    
																	);

												}
								}

								# combine results...
								$interaction[$index] = 
												$interaction_vdw[$index] + $interaction_dd[$index];

								say $interaction_vdw[$index]." --> ".$interaction_dd[$index];

								# increment index...
								$index += 1;

				}

				# set up file name...
				$file_export = "interaction_".$experiment."-".$distance."nm.txt";

				# output the value of interaction...
				write_to_file({array=>\@interaction,file=>$dir.$file_export});
				write_to_file({array=>\@interaction_vdw,file=>$dir."vdw_".$file_export});
				write_to_file({array=>\@interaction_dd,file=>$dir."ele_".$file_export});

}


END {$| = 0}

#===========
# subroutine
#===========

sub rotate_y {
	
	my ($input) = @_;

	my $angle = $input->{angle} // 0.0;
	my $vector = [$input->{vector}] // [[0.0, 0.0, 0.0]];

	my $vector_mat = Math::MatrixReal->new_from_cols($vector);

	my $rot_mat = Math::MatrixReal->new_from_rows(
		[
			[cos($angle), 0.0, sin($angle)],
			[0.0,         1.0, 0.0        ],
			[-sin($angle),0.0, cos($angle)],
		]
	);

	my $new_vector_mat = $rot_mat->multiply($vector_mat);
	return (~$new_vector_mat)->[0]->[0];
}

sub dd {
	my ($p1, $p2, $idl) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];
	$idl //= 0.0; # inverse debye length...

	return exp(-1*$idl*dist($p1,$p2)) 
				 * 1.0 
				 / dist($p1,$p2);
}

# vdw interaction...
sub vdw {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return dist($p1,$p2)**(-6);
}

sub dist {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return 
		sqrt 
		sum
		pairwise { ($a-$b)**2 } @{$p1}, @{$p2};
}

sub cylinder_coordinates_gen {

	# set up the input...
	my ($input) = @_;     # input with hash...

	# parameter setup...
	my $d = $input->{grid_size}   // 1.4;       # the grid_size between points...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;       # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my @rectangle = ();  # coordinates of rectangle grid...

	# figure out properties of cylinders...
	my $n = floor(2*$R/$d); # max number of points along radius... 
	my $m = floor($H/$d); # number of layers... 

	# need an array to store coordinates for all 
	# candidate coordinates horizontally...
	my @coordinates = ();

	# at least this many number of points are needed...
	foreach my $index (0..$n) {  
		push @coordinates, (-$n/2+$index)*$d;
	}

	# coordinates vertically...
	my @heights = ();
	foreach my $index (0..$m) {
		push @heights, (-$m/2+$index)*$d;
	}

	# say Dumper $C;

	# pick up coordinates from @coordinates 
	# and form array rectangle...
	foreach my $height (@heights) {
		foreach my $row (@coordinates) {
			push @rectangle, [           # each array reference here represents
			                             # coordinate of a point...
				map {
					$_**2 + $row**2 < $R**2 ?   # filter those which are not in the circle...

					[
						pairwise {$a+$b}       # pairwise addition from two arrays below...
						@{[$_,$row,$height]},  # dereference version:sandwich version of array...
						@{$C}
					]        :

					[ ]                      # otherwise, return an empty array reference...
				} 
				@coordinates

			];
		}
	}

	# print information about system for analysis...
#	say "Number of layers => ", $m+1;

	push @{ $input->{cylinder} }, 
			 grep { scalar @{ $_ } > 0 } # remove empty list...
       flat @rectangle;

	return 0;
}

sub shell_coordinates_gen {

	# set up input...
	my ($input) = @_;

	# parameter setup...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;                        # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my $N = $input->{points}     // 100;
	my @rectangle = ();  # coordinates of rectangle grid...

	# need an array to store coordinates for all 
	my @coordinates = ();
	my $angle = 0;

	foreach my $id (0..$N-1) {
		$angle = rand(1)*2*pi;
		push @coordinates, [
			pairwise {$a+$b} 
				@{[
					$R * cos($angle),
					$R * sin($angle),
					$H*rand(1) - $H/2, 
				]},
				@{$C}
		];
	}

	# output...
	push @{ $input->{shell} }, @coordinates;

	return 0;
}

# [====....]
sub coordinates_move {
				
				my ($input) = @_;
				
				my $current_coordinates = $input->{coordinates} // [];    # 
				my $path_vector = $input->{path_vector} // [0,0,0];

				# move by vector...
				foreach my $point (@{ $current_coordinates }) {
								
								$point = [
												pairwise {$a+$b} 
																@{ $point }, @{ $path_vector }
								];

				}

}

sub write_to_file {
	
		my ($input) = @_;

		my $array_rf   = $input->{array};         # input array...
		my $write_file = $input->{file};          # file to write... 

		open (my $write_fh, ">".$write_file);

		my $current_data = undef;
		foreach my $current_item (@{$array_rf}) {
				
				$current_data = is_arrayref($current_item)?
												join " ", @{$current_item} :
												$current_item;

				# if it is a reference...
				print $write_fh $current_data."\n";

		}

		close($write_fh)

}
