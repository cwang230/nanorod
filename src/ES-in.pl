#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                      # floor function...
use List::MoreUtils 'pairwise'; # pairwise addition...
use Data::Dumper;               # show data hierarchy...
use List::Util qw(sum reduce);
use List::Flatten;
use File::Slurp;                # enable file slurp during input...
use Math::MatrixReal;
use Math::Trig;

#=========================================
# (:-> 
#  d:distance between two grid points,
#  R:radius of the circle,
#  C:center of the circle,
#  H:heght of the circle,
#  s:separation between two rods,
# )^<setup>
# -->
# n:number of intervals between two points horizontally
# = (2*R/d)._<floor>
# m:number of intervals between two points vertically
# = (H/d)._<floor>
#=========================================

#==========
# main code
#==========

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Generate coordinates for two cylinders...
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

my $cylinder_left = [];
my $cylinder_right = [];
my %separations = (
	in => 8.8,
	out => 5.4,
);
my $separation = $separations{in};
# my $separation = $separations{out};
my $radius = 17.0/2.0;
my $height = 22.0;
my $distance = 1.4;
my $center_left = [0.0, -1*$separation/2.0-$radius, 0.0];
my $center_right = [0.0, 1*$separation/2.0+$radius, 0.0];
my %idls = (      # inverse Debye length...
	in => 1.95221,
	out => 0.52464,
);
my $idl = $idls{in};
my $hmk = 3e-19; # Hamaker constant => J...

cylinder_coordinates_gen( {
		distance => $distance,
		radius => $radius,
		height => $height,
		separation => $separation,
		center => $center_left,
		cylinder => $cylinder_left,
	}
);

cylinder_coordinates_gen( {
		distance => $distance,
		radius => $radius,
		height => $height,
		separation => $separation,
		center => $center_right,
		cylinder => $cylinder_right,
	}
);

# electric constants...
my $epsilon0 = 8.85e-21;
my %epsilon = (
	in  => 80,
	out => 40,
);
my $pts_per_cylinder = scalar @{ $cylinder_left };
my $sigma = 2e-19 * $height / $pts_per_cylinder;
my $joule2eV = 6.242e18;
my %epss = (
	in  => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{in})**(-1),
	out => (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon{out})**(-1),
);
my $eps = $epss{in};

# say 'Number of pts = ', $pts_per_cylinder;
# say '$eps{in} = ', $eps{in};
# say '$eps{out} = ', $eps{out};

# say Dumper $cylinder_left;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Compute interactions between two cylinders...
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#==============================================================
# read array data...
# parameters:
# left : Array for structure on the left...
# right : Array for structure on the right...
# height : height of the structrue == length of single array...
# area : number of sample points in a single layer...
#==============================================================

# set up two cylinders...
my @cylinder_1 = @{ $cylinder_left };
my @cylinder_2 = @{ $cylinder_right };

# # rotate them if necessary...
# my $theta = 0.0; 
# foreach my $p1 (@{$cylinder_right}) {
# 	# rotate_y({angle=>pi/4.0,vector=>$cylinder_right->[$p1]});
# 	$p1 = rotate_y({angle=>pi/10.0,vector=>$p1});
# }
  
# open(my $fh, ">coords_right_test.txt");
# # open(my $fh, ">coords_left.txt");
# foreach my $L1 (@{$cylinder_right}) {
# 			(say $fh join ' ', @$L1) if @$L1 != 0;
# }
# close($fh);

#--------
# compute
#--------

# set up the sampling angle...
my @angles = map {$_*pi/180}    # convert it from degree to radius...
             grep {$_%5==0}     # choose angle by every 5 degree from 0 degree...
						 (0..90);           # set up the sampling angle...

# keep track of interaction...
my @interaction = ();
my $dv = pi*$radius**2*$height/$pts_per_cylinder;
my $index = 0;

foreach my $angle (@angles) {

	# use the original coordinates...
	@cylinder_2 = @{ $cylinder_right };
	
	# rotate one cylinder...
	foreach my $pt (@cylinder_2) {
		$pt = rotate_y({angle=>$angle,vector=>$pt});
	}

	# compute the interaction...
	foreach my $p1 (@cylinder_1) {
		foreach my $p2 (@cylinder_2) {

			# interactions...
			$interaction[$index] += 
				$joule2eV * (
					$eps*dd($p1,$p2,$idl) +               # dd interaction...    
					(-1)*pi**(-2)*$hmk*vdw($p1,$p2)          # vdw interaction...
				) * ($dv**2);

		}
	}

	# increment index...
	$index += 1;

}

# output the value of interaction...
open(my $fh, ">inter_2.txt");
foreach my $L1 (@interaction) {
			say $fh $L1;
}
close($fh);


#===========
# subroutine
#===========

sub rotate_y {
	
	my ($input) = @_;

	my $angle = $input->{angle} // 0.0;
	my $vector = [$input->{vector}] // [[0.0, 0.0, 0.0]];

	my $vector_mat = Math::MatrixReal->new_from_cols($vector);

	my $rot_mat = Math::MatrixReal->new_from_rows(
		[
			[cos($angle), 0.0, sin($angle)],
			[0.0,         1.0, 0.0        ],
			[-sin($angle),0.0, cos($angle)],
		]
	);

	my $new_vector_mat = $rot_mat->multiply($vector_mat);
	return (~$new_vector_mat)->[0]->[0];
}

# my $another = Math::MatrixReal
# 			        ->new_from_rows($cylinder_left)
# 							->[0];            # get the array reference back...
# 
# say Dumper $another;

# dipole dipole interaction...
sub dd {
	my ($p1, $p2, $idl) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];
	$idl //= 0.0; # inverse debye length...

	return exp(-1*$idl*dist($p1,$p2)) 
				 * 1.0 
				 / dist($p1,$p2);
}

# vdw interaction...
sub vdw {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return dist($p1,$p2)**(-6);
}

sub dist {
	my ($p1, $p2) = @_;
	
	# set default value to empty array referenece if there's no input coordinates...
	$p1 //= [];   
	$p2 //= [];

	return 
		sqrt 
		sum
		pairwise { ($a-$b)**2 } @{$p1}, @{$p2};
}

sub cylinder_coordinates_gen {

	# set up the input...
	my ($input) = @_;     # input with hash...

	# parameter setup...
	my $d = $input->{distance}   // 1.4;       # the distance between points...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;       # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my @rectangle = ();  # coordinates of rectangle grid...

	# figure out properties of cylinders...
	my $n = floor(2*$R/$d); # max number of points along radius... 
	my $m = floor($H/$d); # number of layers... 

	# need an array to store coordinates for all 
	# candidate coordinates horizontally...
	my @coordinates = ();

	# at least this many number of points are needed...
	foreach my $index (0..$n) {  
		push @coordinates, (-$n/2+$index)*$d;
	}

	# coordinates vertically...
	my @heights = ();
	foreach my $index (0..$m) {
		push @heights, (-$m/2+$index)*$d;
	}

	# say Dumper $C;

	# pick up coordinates from @coordinates 
	# and form array rectangle...
	foreach my $height (@heights) {
		foreach my $row (@coordinates) {
			push @rectangle, [           # each array reference here represents
			                             # coordinate of a point...
				map {
					$_**2 + $row**2 < $R**2 ?   # filter those which are not in the circle...

					[
						pairwise {$a+$b}       # pairwise addition from two arrays below...
						@{[$_,$row,$height]},  # dereference version:sandwich version of array...
						@{$C}
					]        :

					[ ]                      # otherwise, return an empty array reference...
				} 
				@coordinates

			];
		}
	}

	# print information about system for analysis...
#	say "Number of layers => ", $m+1;

	push @{ $input->{cylinder} }, 
			 grep { scalar @{ $_ } > 0 } # remove empty list...
       flat @rectangle;

	return 0;
}

sub shell_coordinates_gen {

	# set up input...
	my ($input) = @_;

	# parameter setup...
	my $R = $input->{radius}     // 17.0/2.0;
	my $H = $input->{height}     // 22.0;
	my $s = $input->{separation} // 8.8;       # separation between two rods...
	my $C = $input->{center}     // [0.0, -1*$s/2.0-$R ,0.0];
	my $N = $input->{number of points}   // 100;
	my @rectangle = ();  # coordinates of rectangle grid...

	# figure out properties of cylinders...
	my $n = floor(2*$R/$d); # max number of points along radius... 
	my $m = floor($H/$d); # number of layers... 

	# need an array to store coordinates for all 
	my @coordinates = ();
	my $angle = 0;

	foreach my $id (0..$N-1) {
		$angle = rand(1)*2*pi;
		push @coordinates, [
			pairwise {$a+$b} 
				(
					$R * cos($angle),
					$R * sin($angle),
					$H*rand(1) - $H/2, 
				),
				@{$C}
		];
	}

	# output...
	push @{ $input->shell }, @coordinates;

	return 0;
}
