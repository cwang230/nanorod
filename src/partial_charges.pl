#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use List::MoreUtils 'pairwise'; # pairwise addition...
use Math::Trig;
use Math::Orthonormalize qw(normalize);
use Data::Dumper;
use Math::Vector::Real qw(V);
use Math::Vector::Real::Random;
use Data::Structure::Util qw(unbless);
use Nanorod;


my $random_points = [];
my $DEBUG = 0;
if ($DEBUG == 1) {
foreach (1..1000) {
				push @{$random_points}, 
#								normalize (
												unbless(Math::Vector::Real->random_in_sphere(3,1))
#								);
}
}

my $init_distance = 5.0;
my $final_distance = 6.0;
my $ligand_size = 2.00;  # nm
my $separation_shell = $init_distance - 2*$ligand_size;
my $separation = $init_distance;
my $radius = 17.0/2.0;  # nm
my $height = 22.0;      # nm
my $grid_size = 1.4;    # nm
my %center = (
				# left  => [0.0, -1*$separation/2.0-$radius, 0.0],
				# right => [0.0,  1*$separation/2.0+$radius, 0.0],
				left  => [0.0, 0.0, 0.0],
				right => [0.0, 0.0, 0.0],
);
my $num_rand_shell = 1000;    # total number of random points...           

# generate <shell> and <cylinder>...

my $shell_core_left = [];
my $shell_core_right = [];
shell_coordinates_gen_test({
				radius => $radius + $ligand_size,
				height => $height,
				separation => $separation_shell,
				center => $center{right},
				points => $num_rand_shell,
				shell => $shell_core_right,
});

my $dir = "../results-out/";
write_to_file({array=>$shell_core_right,file=>$dir."samples.txt"});

my $angle = pi/2;
my $end_height = 2.0;
my $range = 2.0;
my $path_vector = [0.0,0.0,2.0];
foreach my $point (@{$shell_core_right}) {

				#; $point = charge_distro_local({                    
				charge_distro_local({                    
								point => \$point,                     
								axis  => "z",                        
    				max_angle => $angle,                 
								max_height => $end_height,          
				});                                      

				# ; last;
				                                        
				; my $DEBUG_RANGE = 0;                  
				; if ($DEBUG_RANGE == 1) {              
				# figure out where to move...           
    $angle = rand(1) * pi/1-pi/1/2;         
				$end_height = $range*rand(1)-$range/2.0;
				$path_vector = [0.0,0.0,$end_height];   
				$point = 
								rotate_axis({
												angle=>$angle,
												vector=>$point,
												axis=>"z"});
				
				# move it...
				$point = translate({
								coordinates => $point,
								path_vector => $path_vector,
				});

				; }

				
}


write_to_file({array=>$shell_core_right,file=>$dir."samples_2.txt"});

# 
# sub charge_distro_local {
# 				
# 				my ($input) = @_;
# 				
# 				my $point      = $input->{point} ;
# 				my $axis       = $input->{axis} // "z";
# 				my $max_angle  = $input->{max_angle} // pi;
# 				my $max_height = $input->{max_height} // 0.0;
# 
# 				# figure out where to move...
# 				my $angle = rand(1) * $max_angle-$max_angle/2;
# 				my $end_height = $max_height*rand(1)-$max_height/2.0;
# 				my $path_vector = [0.0,0.0,$end_height];
# 
# 				# ; $point = 
# 								rotate_axis({
# 												angle=>$angle,
# 												vector=>$point,
# 												axis=>$axis});
# 				
# 				# move it...
# 				#; $point = translate({
# 				translate({
# 								coordinates => $point,
# 								path_vector => $path_vector,
# 				});
# 
# 				return 0;
# 				# return $point;
# }
