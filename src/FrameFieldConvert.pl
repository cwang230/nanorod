#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use Carp;
use Getopt::Long;


# Set up the command line interface...
my $frame;
GetOptions (
	"frame=i" => \$frame,
) or die("Error in command line arguments\n");
say "frame = " . $frame;


my $current_field = Frame2Field(frame=>$frame);
say "field = " . $current_field;


# make subroutine...
sub Frame2Field {
	croak "Missing value for a named arg" if @_ % 2; # @_ = 2 => @_ % 2 -> 0 => if false;...

	my %ags = @_; # convert it to hash format...  
	my $field = $ags{field} // -2000;   # set default value for field...  
	my $step = $ags{step} // 20;       # set default value for step...  
	my $change = $ags{change} // 10;  # set default value for change...  
	my $frame = $ags{frame} // 0;      # set default value for frame...

	# convert...
	my $current_field = 
			int($frame/$step)
				*$change
			+$field 
			
			// 0;

	return $current_field;
}
