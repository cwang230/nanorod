#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use Data::Dumper;
do 'subroutines.pl';

BEGIN {$| = 1}

		my $distance = 8.8;
		my $experiment = "in";

		my %separations = (
			in => $distance,      # nm
			out => $distance,     # nm
		);
		my %idls = (      # inverse Debye length...
			in => 1.38042,   # nm^-1 ... [check]
			out => 0.52464,  # nm^-1 ... [check]
		);
		my $ligand_size = 4.10;  # nm
		# my $ligand_size = 2.00;  # nm

		# program control...
		my $file_export = "interaction_".$experiment."-".$separations{$experiment}."nm.txt";

		my $separation_shell = $separations{$experiment} - 2*$ligand_size;
		my $separation = $separations{$experiment};
		my $radius = 17.0/2.0;  # nm
		my $height = 22.0;      # nm
		my $grid_size = 1.4;    # nm
		my %center = (
			left  => [0.0, -1*$separation/2.0-$radius, 0.0],
			right => [0.0,  1*$separation/2.0+$radius, 0.0],
		);
		my $idl = $idls{$experiment};
		my $hmk = 3e-19; # Hamaker constant => J
		my $num_rand_shell = 1000;

		# generate coordinates...

		my $cylinder_left = [];
		my $cylinder_right = [];
		my $shell_left = [];
		my $shell_right = [];

		shell_coordinates_gen( {
				radius => $radius + $ligand_size,
				height => $height,
				separation => $separation_shell,
				center => $center{left},
				points => $num_rand_shell,
				shell => $shell_left,
			}
		);

		shell_coordinates_gen( {
				radius => $radius + $ligand_size,
				height => $height,
				separation => $separation_shell,
				center => $center{right},
				points => $num_rand_shell,
				shell => $shell_right,
			}
		);

		cylinder_coordinates_gen( {
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{left},
				cylinder => $cylinder_left,
			}
		);

		cylinder_coordinates_gen( {
				grid_size => $grid_size,
				radius => $radius,
				height => $height,
				separation => $separation,
				center => $center{right},
				cylinder => $cylinder_right,
			}
		);

print Dumper $cylinder_left->[0];
cylinder_coordinates_move({coordinates=>$cylinder_left,path_vector=>[0,0.1,0]});
print Dumper $cylinder_left->[0];


print Dumper $shell_left->[0];
cylinder_coordinates_move({coordinates=>$shell_left,path_vector=>[100,0.1,0]});
print Dumper $shell_left->[0];

END {$| = 0}
