#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                      # floor function...
use List::MoreUtils 'pairwise'; # pairwise addition...
use Data::Dumper;               # show data hierarchy...

#=========================================
# (:-> 
#  d:distance between two grid points,
#  R:radius of the circle,
#  C:center of the circle,
# )^<setup>
# -->
# n:number of intervals between two points
# = (R/d)^<floor>
#=========================================

# parameter setup...
my $d = 1.0;
my $R = 13.5;
my @C = (0.0,-6.0,0.0);
my $n = floor($R/$d); # all necessray coordinates... 

# Create an array for rectangle...
my @rectangle = ();  # coordinates of rectangle grid...

# need an array to store coordinates for all 
# candidate coordinates...
my @coordinates = ();

# at least this many number of points are needed...
foreach my $index (0..$n) {  
	push @coordinates, (-$n/2+$index)*$d;
}
#^t: print Dumper(\@coordinates);

# pick up coordinates from @coordinates 
# and form array rectangle...
foreach my $row (@coordinates) {
	push @rectangle, 
		[map {
			$_**2 + $row**2 < $R ?   # filter those which are not in the circle...
			[pairwise {$a+$b} @{[$_,$row,0.0]},@C]        :
			[ ]
	} @coordinates];
}
#^t: print Dumper(\@rectangle);

# That's it...
