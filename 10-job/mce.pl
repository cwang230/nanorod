#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use Data::Dumper;

; my $TEST1 = 0;
; if ($TEST1 == 1) {
use MCE;
my $mce = MCE->new(
				max_workers => 5,
				user_func   => sub {
								my ($mce_foo) = @_;
								$mce_foo->say("Hello from ". $mce_foo->wid);
				}
);
$mce->run;
say "-"x80;
; }

; my $TEST2 = 0;
; if ($TEST2 == 1) {
use MCE::Loop;
use List::Util qw(reduce);

MCE::Loop::init {
				max_workers=> 5,
				chunk_size => 'auto',
};

my %result = mce_loop {
				my ($mce, $chunk_ref, $chunk_id) = @_;
				my %result_foo;
				for my $item (@{ $chunk_ref }) {
								$result_foo{$item} = $item;
								$mce->say ("***".($item)."***");
				}
				$mce->gather(%result_foo);
} 1..10;
my $sum = reduce {$a+$b} values %result;
say $sum;
; }

; my $TEST3 = 0;
; if ($TEST3 == 1) {
use MCE::Loop;
use List::Util qw(reduce);

MCE::Loop::init {
				max_workers=> 4,
				chunk_size => 'auto',
};

my $temp = 0;
my @result = mce_loop {
				my ($mce, $chunk_ref, $chunk_id) = @_;
				my @result_foo;
				for my $item (@{ $chunk_ref }) {
								$mce->say ("***".$temp."+".$item."=".($temp+$item)."***");
								$temp += $item;  # This is a step-dependent operation...
								push @result_foo, $temp;
				}
				$mce->gather(@result_foo);
} 1..10;
my $sum = reduce {$a+$b} @result;
say $sum;
say Dumper \@result;
; }

; my $TEST4 = 1;
; if ($TEST4 == 1) {
use MCE::Loop;
use List::Util qw(reduce);

MCE::Loop::init {
				max_workers=> 10,
				chunk_size => 'auto',
};

sub num_work {
				my ($arg1, $arg2) = @_;

				return ($arg1+$arg2);
}

sub str_work {
				my ($arg1, $arg2) = @_;

				return ($arg1." + ".$arg2);
}

my $array = [
# 				[1,2,3,4],
# 				[5,6,7,8],
				[qw(a1 a2 a3 a4)],
				[qw(b1 b2 b3 b4)],
];
my @result = mce_loop {
				my ($mce, $chunk_ref, $chunk_id) = @_;

				my @result_foo;
				for my $item ($chunk_ref->@*) {
								for my $item2 ($array->[1]->@*) {
												$mce->say ("***".($item)." && ".$item2."***");
												push @result_foo, 
																str_work($item,$item2);
								}
				}
				$mce->gather(@result_foo);
} $array->[0]->@*;
# my $sum = reduce {$a+$b} @result;
# say $sum;
# say Dumper \@result;
; }
