#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use Parallel::ForkManager;

my $pm = Parallel::ForkManager->new(10);

my @jobs = map {"7".$_."-main_in.pl"} (0..9);

foreach my $id (0..9) {
				my $pid = $pm->start and next;
				system("perl ".$jobs[$id]." > 07_log_".$id) == 0 or die("failed due to ".$?);
				$pm->finish;
}

$pm->wait_all_children;

