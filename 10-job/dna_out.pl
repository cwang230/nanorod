#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use Math::Trig;
use Data::Dumper;
use Ref::Util qw(is_arrayref);

# elastic dna...

my @energies;

my @lengths = map {$_/10.0} (50..59);
# my @lengths = map {$_/10.0} (82..90);
my @angles = map {$_*pi/180.0} grep {$_%1==0} (-45..45);
# my @angles = map {$_*pi/180.0} grep {$_%1==0} (-90..90);
foreach my $length (@lengths) {

		push @energies, [
				map {dna({length=>$length,angle=>$_,type=>"out"})} 
				(@angles)
		];

}

# print Dumper \@energies;
# print scalar @energies;

write_to_file({array=>\@energies,file=>"../96-new-results-out/dna_twisting_out_1.txt"});

sub dna {
		
		my ($input) = @_;

		my $l1 = $input->{length};
		my $theta = $input->{angle};
		my $type  = $input->{type};

		my $c = 480; my $g = 120; my $s = 600.0;
		my %idls = (         # inverse Debye length...
						in => 1.38042,   # nm^-1             
						out => 0.52464,  # nm^-1             
		);                                       
		# my $l0 = 4.0;                 # assume it's A form DNA, 2.6*15 ~~ 4nm...
		my $l0 = 4.9;                   # assume it's B form DNA, 5nm...
		my $x = $l1 - $l0;

		my @coefs = (                   # Coefficents, unit: pN * nm
				0.5*$c/$l0,                 
				$g*$x/$l0,
				0.5*$s*$x**2/$l0,
		);                                

		# conversion...                                
		my $rate = 6.24e-3;             # pN*nm --> eV;

		# energy...
		my $energy =    $coefs[0]*$theta**2 
																+ $coefs[1]*$theta
																+ $coefs[2];
		$energy *= $rate;

		return $energy;
} 


sub write_to_file {
	
		my ($input) = @_;

		my $array_rf   = $input->{array};         # input array...
		my $write_file = $input->{file};          # file to write... 

		open (my $write_fh, ">".$write_file);

		my $current_data = undef;
		foreach my $current_item (@{$array_rf}) {
				
				$current_data = is_arrayref($current_item)?
												join " ", @{$current_item} :
												$current_item;

				# if it is a reference...
				print $write_fh $current_data."\n";

		}

		close($write_fh)

}
