#!/usr/bin/env perl 

# heads ...
use strict;
use warnings;
use feature qw(say);
use POSIX;                           # floor function...                      
use List::MoreUtils 'pairwise';      # pairwise addition...                   
use Data::Dumper;                    # show data hierarchy...                 
use List::Util qw(sum reduce);                                                
use List::Flatten;                                                            
use File::Slurp;                     # enable file slurp during input...      
use Math::MatrixReal;                                                         
use Math::Trig;                                                               
use Ref::Util qw(is_arrayref);       # test if object is an array reference...
use Nanorod;
use MCE::Loop;
use Benchmark;

BEGIN {$| = 1}

# set up the initial distance for initial coordinates of cylinders and shells...

# set up cylidner, shell coordinates
#-----------------------------------
                                         
# parameters...

my $num_rand_shell = 14*18;    # total number of random points...           
my $init_distance = 50;
my $final_distance = 50;

my $ligand_size = 0.00;  # nm
my $dim = {                               
				x => 420,        # unit => nanometer
				y => 420,                           
				z => 830,
};
my $size_hollow = 78;
my $grid_size = 30.0;    # nm
my $separation = 50.0;
my %center = (
				left  => [0.0, -1*$separation/2.0-$dim->{y}, 0.0],
				right => [0.0,  1*$separation/2.0+$dim->{y}, 0.0],
);

# set up coordinates for the hollow rod...
my $rec_coords = {
				left  => [],
				right => [],
};
rec_coordinates_gen({
				x => $dim->{x},
				y => $dim->{y},
				z => $dim->{z},
				hollow => $size_hollow,
				center=> $center{left},
				coords => $rec_coords->{left},
				step  => $grid_size,
});
rec_coordinates_gen({
				x => $dim->{x},
				y => $dim->{y},
				z => $dim->{z},
				hollow => $size_hollow,
				center=> $center{right},
				coords => $rec_coords->{right},
				step  => $grid_size,
});
# say Dumper $rec_coords;

my $cha_coords = {
				left  => [], 
				right => [],
};
rec_coordinates_gen({
				x => $dim->{x} + 1.2*$grid_size,
				y => $dim->{y} + 1.2*$grid_size,
				z => $dim->{z},
				hollow => $grid_size,
				center=> $center{left},
				coords => $cha_coords->{left},
				step  => $grid_size,
});
rec_coordinates_gen({
				x => $dim->{x} + 1.2*$grid_size,
				y => $dim->{y} + 1.2*$grid_size,
				z => $dim->{z},
				hollow => $grid_size,
				center=> $center{right},
				coords => $cha_coords->{right},
				step  => $grid_size,
});

# set up output folder...
my $dir = "../11-results/";
my $file_export = "";

# output the configuration...
write_to_file({array=>$rec_coords->{left},file=>$dir."rec_coords_left.txt"});
write_to_file({array=>$rec_coords->{right},file=>$dir."rec_coords_right.txt"});
write_to_file({array=>$cha_coords->{left},file=>$dir."cha_coords_left.txt"});
write_to_file({array=>$cha_coords->{right},file=>$dir."cha_coords_right.txt"});

#===========================
# code mute
#===========================

#--------------------------
# set up specs for solution
#--------------------------
                                              
my $idl = 0.52464;
my $hmk = 3e-19; # Hamaker constant => J
                                         
# electric constants...
my $epsilon0 = 8.85e-21;   # F nm^-1...
my $epsilon = 60;
my $pts_per_rod = scalar @{ $rec_coords->{left} };
my $sigma = 1.6e-19;    
my $joule2eV = 6.242e18;   # conversion between joule to eV...
my $eps = (4*pi*$epsilon0)**(-1) * $sigma**2 * ($epsilon)**(-1);

#***************************************
# extra work for the boring distribution
#***************************************
 
my $weighted_num_charges = 0;
my $weighted_charge_density = 0;
$weighted_num_charges = scalar @{ $cha_coords->{left} } ;
$weighted_charge_density = $num_rand_shell / $weighted_num_charges;

#----------
# compute  
#----------

# set up the sampling distances...
my @distances = map {$_/10.0} ($init_distance*10)..($final_distance*10);

# set up the sampling angles...
my @angles = map {$_*pi/180}    # convert it from degree to radius...
					         grep {$_%5==0}    # choose angle by every 1 degree from 0 degree...
					         (-90..90);        # set up the sampling angle...

my %path_vector = (
				left  => [],
				right => [],
);

# keep track of interaction...
my @interaction_vdw = ();
my @interaction_dd = ();
my @interaction = ();
my $dv = ($dim->{x}**2 - (2*$size_hollow)**2)*$dim->{z} / $pts_per_rod;
my $index = 0;                   # layer id...

# set up two cylinders, two shells...
my @rod_1 = ();
my @rod_2 = ();
my @cha_1 = ();
my @cha_2 = ();

; my $DEBUG = 1;

; if ($DEBUG == 1) {  # __DEBUG__

# for the copy of points on cha_2...
my $p2_cp = [];
my $unit_ele = 0;

#===========
# launch MCE
#===========
MCE::Loop::init {
				max_workers => 64,
				chunk_size  => 'auto',
};

my $ctrl_index = 0;
my $file_index = 0;
my $PARALLEL   = 1;
ONE_JOB:
foreach my $distance (@distances) {

				if ($ctrl_index != $file_index) {
								$ctrl_index += 1;
								next ONE_JOB;
				}

				# get the path vector...
				$path_vector{left} = [0, -($distance-$init_distance)/2.0, 0];
				$path_vector{right} = [0, ($distance-$init_distance)/2.0, 0];

				# separate cylinders more...
				# CAUTION, IT MIGHT NOT BE CORRECT...
				coordinates_move({
								coordinates=>$rec_coords->{left},
								path_vector=>$path_vector{left},
				});
				coordinates_move({
								coordinates=>$rec_coords->{right},
								path_vector=>$path_vector{right},
				});
				coordinates_move({
								coordinates=>$cha_coords->{left},
								path_vector=>$path_vector{left},
				});
				coordinates_move({
								coordinates=>$cha_coords->{right},
								path_vector=>$path_vector{right},
				});

				say "compute ".$distance."...";

				# get a local copy of coordinates...
				@rod_1 = @{ $rec_coords->{left} };
				@rod_2 = @{ $rec_coords->{right} };
				@cha_1 = @{ $cha_coords->{left} };
				@cha_2 = @{ $cha_coords->{right} };

				$index = 0;

				foreach my $angle (@angles) {

								# pick up one cylinder...
								@rod_2 = @{ $rec_coords->{right} };

								# rotate the cylinder...
								foreach my $pt (@rod_2) {
												$pt = rotate_y({angle=>$angle,vector=>$pt});
								}

								# pick up one shell...
								@cha_2 = @{ $cha_coords->{right} };

								# rotate one shell...
								foreach my $pt (@cha_2) {
												$pt = rotate_y({angle=>$angle,vector=>$pt});
								}

								# compute the interaction_vdw...
								$interaction_vdw[$index] = 0;

								#==========
								# benchmark
								#==========
								my $t0 = Benchmark->new;

								; if ($PARALLEL == 0) {
								foreach my $p1 (@rod_1) {
												foreach my $p2 (@rod_2) {

																# interaction_vdw calculated...
																$interaction_vdw[$index] += 
																	$joule2eV * (
																		(-1)*pi**(-2)*$hmk*vdw($p1,$p2)          # vdw interaction_vdw...
																	) * ($dv**2);

												}
								}
								; } else {
								my @vdw_accum = mce_loop {
												my ($mce, $chunk_ref, $chunk_id) = @_;

												my @vdw_accum_mce;
												for my $p1 ($chunk_ref->@*) {
																for my $p2 (@rod_2) {

																				# interaction_vdw calculated...
																				push @vdw_accum_mce, 
																					$joule2eV * (
																						(-1)*pi**(-2)*$hmk*vdw($p1,$p2)          # vdw interaction_vdw...
																					) * ($dv**2);

																}
												}
												$mce->gather(@vdw_accum_mce);
								} @rod_1;
								$interaction_vdw[$index] = reduce {$a+$b} @vdw_accum;
								; }

								say "-"x80;

								# compute the interaction_dd...
								$interaction_dd[$index] = 0;

								; if ($PARALLEL == 0) {
								foreach my $p1 (@cha_1) {

												CHARGE:
												foreach my $p2 (@cha_2) {

																# cutoff to consider electrostatic...
																if (dist($p1,$p2) > 4.0) {next CHARGE;}

																# copy of $p2 for figuring out charge...
																$p2_cp = [@{ $p2 }];  # ref to array of numbers...
																rotate_axis({
																				angle => -$angle,
																				vector=> \$p2_cp,
																				axis  => "y",
																});

																# interaction_dd calculated...
																$unit_ele =
																				$joule2eV            * 
																				$eps*dd($p1,$p2,$idl);         # dd interaction_vdw...

																$unit_ele *=						 
																				$weighted_charge_density *
																				$weighted_charge_density ;

																$interaction_dd[$index] += $unit_ele;

												}
								}
								; } else {
								my @cha_accum = mce_loop {
												my ($mce, $chunk_ref, $chunk_id) = @_;

												my @cha_accum_mce = (0.0);
												for my $p1 ($chunk_ref->@*) {
																CHARGEPARALLEL:
																for my $p2 (@cha_2) {

																				# cutoff to consider electrostatic...
																				if (dist($p1,$p2) > 4.0) {
																								next CHARGEPARALLEL;
																				}

																				# copy of $p2 for figuring out charge...
																				$p2_cp = [@{ $p2 }];  # ref to array of numbers...
																				rotate_axis({
																								angle => -$angle,
																								vector=> \$p2_cp,
																								axis  => "y",
																				});

																				my $unit_ele_para = 0;
																				# interaction_dd calculated...
																				$unit_ele_para =
																								$joule2eV            * 
																								$eps*dd($p1,$p2,$idl);         # dd interaction_vdw...

																				$unit_ele_para *=						 
																								$weighted_charge_density *
																								$weighted_charge_density ;

																				; say $unit_ele_para;

																				# interaction_vdw calculated...
																				push @cha_accum_mce, $unit_ele_para;
																}
												}
												$mce->gather(@cha_accum_mce);
								} @cha_1;
								$interaction_dd[$index] = reduce {$a+$b} @cha_accum;
								; }

								my $t1 = Benchmark->new;
								my $td = timediff($t1,$t0);

								say "----> ", timestr($td);

								# combine results...
								$interaction[$index] = 
												$interaction_vdw[$index] + $interaction_dd[$index];

								say $interaction_vdw[$index]." --> ".$interaction_dd[$index];

								# increment index...
								$index += 1;

				}

				# set up file name...
				$file_export = "interaction_-".$distance."nm.txt";

				# output the value of interaction...
				write_to_file({array=>\@interaction,file=>$dir.$file_export});
				write_to_file({array=>\@interaction_vdw,file=>$dir."vdw_".$file_export});
				write_to_file({array=>\@interaction_dd,file=>$dir."ele_".$file_export});

				last ONE_JOB;
}

; } # __DEBUG__

END {$| = 0}


