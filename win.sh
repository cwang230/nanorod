#!/usr/bin/env bash

i3-msg 'workspace new'
i3-msg 'rename workspace to 01-chiara-code'
i3-msg 'exec gnome-terminal --working-directory=/shared/noe5_data1/scott/00-Chiara-Rod-Mean_field/20-job/'

i3-msg 'workspace new'
i3-msg 'rename workspace to 02-chiara-results'
i3-msg 'exec gnome-terminal --working-directory=/shared/noe5_data1/scott/00-Chiara-Rod-Mean_field/20-results/'

i3-msg 'workspace new'
i3-msg 'rename workspace to 03-chiara-code'
i3-msg 'exec gnome-terminal --working-directory=/shared/noe5_data1/scott/00-Chiara-Rod-Mean_field/30-job/'

i3-msg 'workspace new'
i3-msg 'rename workspace to 04-chiara-results'
i3-msg 'exec gnome-terminal --working-directory=/shared/noe5_data1/scott/00-Chiara-Rod-Mean_field/30-results/'
